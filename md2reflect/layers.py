import numpy as np
import periodictable


def calculate_substrate_SLD(substrate_element, probe="neutrons", xray_energy=8):
    """Computes the neutron or x-ray scattering length density of any element.

    This function computes the real and imaginary parts of the scattering
    length density, either for neutrons or for x-rays, of any element in normal
    conditions. Although, typically it is used to determine the scattering
    length density of the substrate layer, which is added manually in the
    appropriate side of the simulation scattering length density.

    It also displays other information of the element, such as mass density,
    atomic mass, number density, and scattering length (both real and imaginary
    components).

    X-ray scattering lengths of materials are dependent on the energy of the
    incident x-rays. If none is specified, the default is 8 keV, which
    corresponds to copper, a common element for building x-ray sources.

    Parameters:
        substrate_element (str): Name of the element of the substrate, such as
            "copper", "silicon", or the like.
        probe (str): "neutrons" or "xrays", to specify the nature of the beam.
            Default value is "neutrons".
        xray_energy (float): The energy of the incident x-rays in keV. Default
            value is 8 keV, which corresponds approximately to a copper source.

    Returns:
        SLD_substrate (tuple): Real and imaginary components of the neutron or
        x-ray scattering density (depending on which probe was selected).
        Imaginary components are associated with the absorption.


    Example:
    ::

        calculate_substrate_SLD("silicon")

        >>>  Substrate:
            Density = 2.34 g/cm^3
            Mass = 10.811 g/mol

            Number density = 0.13034697797243547 Å^-3
            bc = ( 5.3 -i 0.21 ) fm
            SLD substrate = ( 6.90838983253908e-06
                             -i 2.737286537421145e-07 ) Å^-2

            (6.90838983253908e-06, -2.737286537421145e-07)
    """
    e_radius = periodictable.constants.electron_radius

    select_periodictable = getattr(periodictable, substrate_element)

    if probe == "neutrons":
        br_substrate = select_periodictable.neutron.b_c  # in fm
        bi_substrate = select_periodictable.neutron.b_c_i  # in fm

        if bi_substrate is None:
            bi_substrate = 0.0

    elif probe == "xrays":
        f1, f2 = select_periodictable.xray.scattering_factors(energy=xray_energy)
        br_substrate = e_radius * f1 * 10 ** 15  # in fm
        bi_substrate = -e_radius * f2 * 10 ** 15  # in fm

    density_substrate = select_periodictable.density  # in g/cm^2
    mass_substrate = select_periodictable.mass  # in g/mol

    N_AVOGADRO = periodictable.constants.avogadro_number
    number_density_substrate = (
        N_AVOGADRO * density_substrate / mass_substrate
    ) * 10 ** (
        -24
    )  # in Å^-3

    # in Å^-2
    SLD_substrate_r = number_density_substrate * br_substrate * 10 ** (-5)
    SLD_substrate_i = number_density_substrate * bi_substrate * 10 ** (-5)

    if bi_substrate < 0.0:
        sign = "-"
    elif bi_substrate >= 0.0:
        sign = "+"

    print(" Substrate:")
    if probe == "xrays":
        print(f"X-rays energy = {xray_energy} keV")
    print(f"Density = {density_substrate} g/cm^3")
    print(f"Mass = {mass_substrate} g/mol\n")

    print(f"Number density = {number_density_substrate} Å^-3")
    print(f"bc = ( {br_substrate} {sign}i {abs(bi_substrate)} ) fm")
    print(f"SLD substrate = ( {SLD_substrate_r} {sign}i {abs(SLD_substrate_i)} ) Å^-2")

    return (SLD_substrate_r, SLD_substrate_i)


def build_substrate_layer(substrate_element, probe="neutrons", xray_energy=8):
    """Constructs a substrate layer that can be added to the simulated SLD.

    This function computes the real and imaginary parts of the scattering
    length density, either for neutrons or for x-rays, of any element in normal
    conditions and outputs a tuple that can be directly fed to the building
    functions (:func:`build_SLD_left` and :func:`build_SLD_right`), where the
    scattering length density of the simulation trajectory is combined with
    other theoretical layers added manually in order to calculate the
    reflectivity.

    That is, a list with a tuple with the real part of the scattering lenght
    density, the imaginary part of the scattering length density, the
    thickness, and the roughness.

    Here, the thickness is set to zero, which is approppriate for the substrate
    layer, to use this for other layers the thickness parameter has to be
    changed to another value.

    It also displays other information of the element, such as mass density,
    atomic mass, number density, and scattering length (both real and imaginary
    components).

    X-ray scattering lengths of materials are dependent on the energy of the
    incident x-rays. If none is specified, the default is 8 keV, which
    corresponds to copper, a common element for building x-ray sources.

    Parameters:
        substrate_element (str): Name of the element of the substrate, such as
            "copper", "silicon", or the like.
        probe (str): "neutrons" or "xrays", to specify the nature of the beam.
            Default value is "neutrons".
        xray_energy (float): The energy of the incident x-rays in keV. Default
            value is 8 keV, which corresponds approximately to a copper source.

    Returns:
        substrate_layers (list of tuple): List with a single tuple element
        that contains the information of a material layer. In this case the
        substrate. More layers can be later added between this substrate layer
        and the simulation layers. The first one is the semi-infinite layer and
        its thickness should be set to zero.

    Example:
    ::

        build_substrate_layer("silicon")

        >>> Substrate:
            Density = 2.33 g/cm^3
            Mass = 28.0855 g/mol

            Number density = 0.04996026551316515 Å^-3
            bc = ( 4.15071 +i 0.0 ) fm
            SLD substrate = ( 2.0737057366814975e-06 +i 0.0 ) Å^-2

            [(2.0737057366814975e-06, 0.0, 0.0, 0.0)]

        # This corresponds to [(SLD, absorption SLD, layer thickness,
        # roughness)].
    """
    SLD_substrate = calculate_substrate_SLD(
        substrate_element, probe=probe, xray_energy=xray_energy
    )
    substrate_layers = [(SLD_substrate[0], SLD_substrate[1], 0.0, 0.0)]
    return substrate_layers


def average_environment_SLD(
    SLD_total_no_substrate, zbin_from, zbin_to, distance_input=False, zbin_interval=None
):
    """Computes the average SLD of the simulation trajectory in a certain range.

    This function takes the specified interval of the scattering length density
    of the simulation, and computes its average.

    In many systems where a layered structure arises near the interface but it
    disappears as we move away and reach the bulk, it can be assumed that the
    decreasing oscillations of the simulation SLD would eventually reach a
    constant value.

    The average on a certain range of the simulation SLD is to estimate this
    value. However, how to properly connect the damping oscillations of the
    simulation with this constant is not a trivial issue.

    Parameters:
        SLD_total_no_substrate (tuple of lists): The scattering length density
            of the simulation after adding all the contributions except that
            of the substrate.
        zbin_from (int or float): Starting point of the range to average. It
            can be its z bin index or its position on the z axis. The
            'distance_input' parameter must be set accordingly.
        zbin_to (int or float): Ending point of the range to average. It can be
            its z bin index or its position on the z axis. The 'distance_input'
            parameter must be set accordingly. This point is not included in
            the calculation of the average.
        distance_input (bool): Indicates if the range is indicated by its z bin
            indexs (False, the default) or their position in Å (True).
        zbin_interval (float): The thickness of each bin in Å. Default is None,
            but its value is required if 'distance_input = True'.

    Returns:
        SLD_environment (tuple): Real and imaginary parts of the average
        scattering length density of the indicated range of the simulation.

    Example:
    ::

        average_environment_SLD(SLD_total_no_substrate, 120, 200)

        >>>  Environment:
            SLD environment = ( 8.569835631488211e-07 +i 0.0 ) Å^-2

            (8.569835631488211e-07, 0.0)
    """
    # environment_element = "bulk liquid"

    # Compute the zbins corresponding to the specified distance positions, if
    # necessary.
    if distance_input is True:
        zbin_from = int(zbin_from / zbin_interval)
        zbin_to = int(zbin_to / zbin_interval)

    if len(SLD_total_no_substrate) == 1:
        SLD_environment_r = sum(SLD_total_no_substrate[zbin_from:zbin_to]) / len(
            SLD_total_no_substrate[zbin_from:zbin_to]
        )

        print(" Environment:")
        print(f"SLD environment = ( {SLD_environment_r} ) Å^-2")
        return SLD_environment_r

    elif len(SLD_total_no_substrate) > 1:

        SLD_environment_r = sum(SLD_total_no_substrate[0][zbin_from:zbin_to]) / len(
            SLD_total_no_substrate[0][zbin_from:zbin_to]
        )
        SLD_environment_i = sum(SLD_total_no_substrate[1][zbin_from:zbin_to]) / len(
            SLD_total_no_substrate[1][zbin_from:zbin_to]
        )

        if SLD_environment_i < 0:
            sign = "-"
        elif SLD_environment_i >= 0:
            sign = "+"

        print(
            f"SLD environment = ( {SLD_environment_r} {sign}i {abs(SLD_environment_i)} ) Å^-2",
        )

        return (SLD_environment_r, SLD_environment_i)


def build_average_environmental_layer(
    SLD_total_no_substrate, zbin_from, zbin_to, distance_input=False, zbin_interval=None
):
    """Constructs the environment layer that will be added to the simulated SLD.

    This function takes the specified interval of the scattering length density
    of the simulation, computes its average, and outputs in a format that can
    be directly fed to the building functions (:func:`build_SLD_left` and
    :func:`build_SLD_right`), where the scattering length density of the
    simulation trajectory is combined with other theoretical layers added
    manually in order to calculate the reflectivity. That is, a list with a
    tuple with the real part of the scattering lenght density, the imaginary
    part of the scattering length density, the thickness, and the roughness.

    This combination of the simulation with theoretical layers is normally
    carried out because the reflectivity calculation requires semi-infinite
    layers at both sides, but the simulation is finite.

    With respect to why the average of the simulation SLD, in many systems
    where a layered structure arises near the interface but it disappears as we
    move away and reach the bulk, it can be assumed that the decreasing
    oscillations of the simulation SLD would eventually reach a constant value.
    The average on a certain range of the simulation SLD is to estimate this
    value. However, how to properly connect the damping oscillations with this
    constant is not a trivial issue.

    Here, the thickness is set to zero, which is approppriate for a
    semi-infinite enviornment layer, to use this for other layers the thickness
    parameter should be modified to its corresponding value.

    Parameters:
        SLD_total_no_substrate (tuple of lists): The scattering length density
            of the simulation after adding all the contributions except that
            of the substrate.
        zbin_from (int or float): Starting point of the range to average. It
            can be its z bin index or its position on the z axis. The
            'distance_input' parameter must be set accordingly.
        zbin_to (int or float): Ending point of the range to average. I can be
            its z bin index or its position on the z axis. The
            'distance_input' parameter must be set accordingly.
        distance_input (bool): Indicates if the range is indicated by its z bin
            indexs (False, the default) or their position in Å (True).
        zbin_interval (float) - The thickness of each bin in Å. Default is
            None, but its value is required if 'distance_input = True'.

    Returns:
        environment_layers (list of tuple): List with a single tuple element
        that contains the information of a material layer (real part of the
        scattering length density, imaginary part of the scattering length
        density, thickness, and roughness). In this case the bulk of the
        simulation, which will be the semi-infinite environment layer. More
        layers can be later added between this substrate layer and the
        simulation layers. The first one is the semi-infinite layer and its
        thickness should be set to zero.

    Example:
    ::

        build_average_environmental_layer(SLD_total_no_substrate, 120, 200)

        >>> SLD environment = ( 8.569835631488211e-07 +i 0.0 ) Å^-2

            [(8.569835631488211e-07, 0.0, 0.0, 0.0)]
    """
    # Compute zbins corresponding to the specified distance positions, if
    # necessary.
    if distance_input is True:
        SLD_environment = average_environment_SLD(
            SLD_total_no_substrate,
            zbin_from,
            zbin_to,
            distance_input=True,
            zbin_interval=zbin_interval,
        )
    else:
        SLD_environment = average_environment_SLD(
            SLD_total_no_substrate, zbin_from, zbin_to
        )

    environment_layers = [(SLD_environment[0], SLD_environment[1], 0.0, 0.0)]
    return environment_layers


def build_SLD_left(
    SLD_total_no_substrate,
    first_point_zbin_id,
    last_point_zbin_id,
    substrate_layers,
    environment_layers,
):
    """Builds a layer structure with substrate (left) and environment (right).

    Prepares the layering of the scattering length density so that it can be
    fed to the calculation of the reflectivity profile: it crops the
    scattering length density of the simulation without the substrate
    contribution to the interval of interest, adds the layers specified in
    'substrate_layers' to the left, and adds the layers specified in
    'environment_layers' to the right.

    The cropping is normally necessary to make sure that the different added
    layers are adjoint to the relevant values of the scattering length density
    of the simulation.

    All theoretical layers will be accounted by a single SLD value. So, if any
    intermediate layers are added (layers apart from the two semi-infinite
    ones), their thickness and roughness values will have to be specified when
    calculating the reflectivity by providing the 'substrate_layers' and
    'environment_layers' objects.


    Parameters:
        SLD_total_no_substrate (tuple of lists): Total scattering length
            density of the simulation in each z bin without the contribution
            of the substrate residues/molecules (real and imaginary parts).
        first_point_zbin_id (int): Starting point of the region of interest in
            the scattering length density of the simulation to carry out the
            cropping and add the substrate layers.
        last_point_zbin_id (int): Ending point of the region of interest in
            the scattering length density of the simulation to carry out the
            cropping and add the environment layers.
        substrate_layers (list of tuples): List with one or more tuple elements
            containing the information of each material layer (real part of the
            the scattering length density, imaginary part of the scattering
            length density, thickness, and roughness) that will be added to the
            substrate. The first one is the semi-infinite layer and its
            thickness should be set to zero.
        environment_layers (list of tuples): List with one or more tuple
            elements containing the information of each material layer (real
            part of the scattering length density, imaginary part of the
            scattering length density, thickness, and roughness) that will be
            added to the oposite side to the substrate. The first one is the
            semi-infinite layer and its thickness should be set to zero.

    Returns:
        SLD_tuned_left (tuple of numpy arrays): Constructed scattering length
        density to feed the reflectivity calculation, that combines the
        relevant section of the simulation scattering length density with the
        values of the manually added substrate and environment layers (with
        both, real and imaginary components).

    Example:
    ::

        substrate_layers = [(6.5535e-06, 0.0, 0, 0.),
                            (3.85e-06, 0.0, 10, 0.),
                            (-2.53e-06, 0.0, 8, 0.)]

        environment_layers = (build_average_environmental_layer
                              (SLD_total_no_substrate, 120, 200))

        build_SLD_left(SLD_total_no_substrate, 41, 160, substrate_layers,
                       environment_layers)

        >>> SLD environment = ( 8.569835631488211e-07 +i 0.0 ) Å^-2

            (array([ 6.55350000e-06,  3.85000000e-06, -2.53000000e-06,
                    -5.01792899e-08, -5.62008047e-07, -2.18786747e-06,
                     etc.,
                     1.10371125e-06,  1.13283855e-06,  8.56983563e-07]),
             array([0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
                    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
                    etc.,
                    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]))

        # Note: For the sake of clarity, only the first and last bins of each
        # component are shown here.

    See Also:
        :func:`build_SLD_right`
    """
    SLD_tuned_left_r = []
    SLD_tuned_left_i = []

    # We add the SLD value for all the layers of the substrate in order in a
    # single list where the semiinfinite layer is the first.
    for layer in substrate_layers:
        SLD_tuned_left_r += [layer[0]]
        SLD_tuned_left_i += [layer[1]]

    SLD_environment_layers_stack = [[], []]
    for layer in environment_layers:
        SLD_environment_layers_stack[0] = [layer[0]] + SLD_environment_layers_stack[0]
        SLD_environment_layers_stack[1] = [layer[1]] + SLD_environment_layers_stack[1]

    # We add the the substrate layers at the beginning, then the simulation
    # SLD, and then the environment layers at the end. The first [0] and the
    # last [-1] values correspond to semiinfinite layers.
    SLD_tuned_left_r += (
        SLD_total_no_substrate[0][first_point_zbin_id : last_point_zbin_id + 1]
        + SLD_environment_layers_stack[0]
    )
    SLD_tuned_left_i += (
        SLD_total_no_substrate[1][first_point_zbin_id : last_point_zbin_id + 1]
        + SLD_environment_layers_stack[1]
    )

    # We convert the list to a numpy array
    SLD_tuned_left = (np.array(SLD_tuned_left_r), np.array(SLD_tuned_left_i))

    return SLD_tuned_left


def build_SLD_right(
    SLD_total_no_substrate,
    first_point_zbin_id,
    last_point_zbin_id,
    substrate_layers,
    environment_layers,
):
    """Builds a layer structure with environment (left) and substrate (right).

    Prepares the layering of the scattering length density so that it can be
    fed to the calculation of the reflectivity profile: it crops the
    scattering length density of the simulation without the substrate
    contribution to the interval of interest, adds the layers specified in
    'environment_layers' to the left, and adds the layers specified in
    'substrate_layers' to the right.

    The cropping is normally necessary to make sure that the different added
    layers are adjoint to the relevant values of the scattering length density
    of the simulation.

    All theoretical layers will be accounted by a single SLD value. So, if any
    intermediate layers are added (layers apart from the two semi-infinite
    ones), their thickness and roughness values will have to be specified when
    calculating the reflectivity by providing the 'substrate_layers' and
    'environment_layers' objects.

    Parameters:
        SLD_total_no_substrate (tuple of lists): Total scattering length
            density of the simulation in each z bin without the contribution
            of the substrate residues/molecules (real and imaginary parts).
        first_point_zbin_id (int): Starting point of the region of interest in
            the scattering length density of the simulation to carry out the
            cropping and add the environment layers.
        last_point_zbin_id (int): Ending point of the region of interest in
            the scattering length density of the simulation to carry out the
            cropping and add the substrate layers.
        substrate_layers (list of tuples): List with one or more tuple elements
            containing the information of each material layer (real part of the
            the scattering length density, imaginary part of the scattering
            length density, thickness, and roughness) that will be added to the
            substrate. The first one is the semi-infinite layer and its
            thickness should be set to zero.
        environment_layers (list of tuples): List with one or more tuple
            elements containing the information of each material layer (real
            part of the scattering length density, imaginary part of the
            scattering length density, thickness, and roughness) that will be
            added to the oposite side to the substrate. The first one is the
            semi-infinite layer and its thickness should be set to zero.

    Returns:
        SLD_tuned_right (tuple of numpy arrays): Constructed scattering length
        density to feed the reflectivity calculation, that combines the
        relevant section of the simulation scattering length density with the
        values of the manually added environment and substrate layers (with
        both, real and imaginary components).

    Example:
    ::

        substrate_layers = [(6.5535e-06, 0.0, 0, 0.),
                            (3.85e-06, 0.0, 10, 0.),
                            (-2.53e-06, 0.0, 8, 0.)]

        environment_layers = (build_average_environmental_layer
                              (SLD_total_no_substrate, 120, 200))

        build_SLD_right(SLD_total_no_substrate, 158, 277, substrate_layers,
                       environment_layers)

        >>> SLD environment = ( 8.569835631488211e-07 +i 0.0 ) Å^-2

            (array([ 8.56983563e-07,  6.84689429e-07,  1.10371125e-06,
                     etc.,
                    -2.46031517e-06, -8.22940354e-07, -6.02151479e-08,
                    -2.53000000e-06,  3.85000000e-06,  6.55350000e-06]),
             array([0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
                    etc.,
                    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
                    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]))

        # Note: For the sake of clarity, only the first and last bins of each
        # component are shown here.

    See Also:
        :func:`build_SLD_left`
    """
    SLD_tuned_right_r = []
    SLD_tuned_right_i = []

    # We add the SLD value for all the layers of the substrate in order in a
    # single list where the semiinfinite layer is the last.
    for layer in substrate_layers:
        SLD_tuned_right_r = [layer[0]] + SLD_tuned_right_r
        SLD_tuned_right_i = [layer[1]] + SLD_tuned_right_i

    SLD_environment_layers_stack = [[], []]
    for layer in environment_layers:
        SLD_environment_layers_stack[0] += [layer[0]]
        SLD_environment_layers_stack[1] += [layer[1]]

    # We add the the environment layers at the beginning, then the simulation
    # SLD, and then the substrate layers. The first [0] and the last [-1]
    # values correspond to semiinfinite layers.
    SLD_tuned_right_r = (
        SLD_environment_layers_stack[0]
        + SLD_total_no_substrate[0][first_point_zbin_id : last_point_zbin_id + 1]
        + SLD_tuned_right_r
    )
    SLD_tuned_right_i = (
        SLD_environment_layers_stack[1]
        + SLD_total_no_substrate[1][first_point_zbin_id : last_point_zbin_id + 1]
        + SLD_tuned_right_i
    )

    # We convert the list to a numpy array
    SLD_tuned_right = (np.array(SLD_tuned_right_r), np.array(SLD_tuned_right_i))

    return SLD_tuned_right
