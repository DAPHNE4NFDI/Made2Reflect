from datetime import datetime
import mdtraj as md


def load_trajectory(filenames, filename_topology="", skip_frames=None):
    """Loads one or multiple simulation files into a single trajectory.

    Since it uses the library MDTraj to load the files, it is compatible with a
    large range of trajectory file formats (DCD, PDB...). Some file formats,
    such as DCD files, do not contain topological information about the
    simulation, so an additional file has to be loaded (it can be a single
    frame of the simulation in PDB format).

    Parameters:
        filenames (str or list): Path to the file, or list of paths in the case
            of multiple files.
        filename_topology (str): Path to a file with the topological
            information of the simulation. Is only necessary if the format of
            the trajectory does not include this information.
        skip_frames (int): This parameter allows to skip some frames from the
            simulation, if it is too large. By default it loads all frames.

    Returns:
        traj (trajectory):  Single trajectory with the frames from the
        different files which were loaded.

    Examples:
        ::

            traj=load_trajectory("simulations/traj3_3frames.pdb")

        ::

            traj=load_trajectory(["simulations/traj3_1.pdb",
                                  "simulations/traj3_2.pdb",
                                  "simulations/traj3_3.pdb"])

        ::

            traj=load_trajectory("../simulations/traj3.dcd",
                                 filename_topology="../simulations/traj3_first_frame.pdb",
                                 skip_frames=400)

    """
    time_starting = datetime.now()

    if filename_topology == "":
        traj = md.load(filenames, stride=skip_frames)
    else:
        traj = md.load(filenames, top=filename_topology, stride=skip_frames)

    print(traj)
    print(traj.time)  # picoseconds (?)
    print(f"Loading time: {datetime.now() - time_starting} (hh:mm:ss)")
    return traj


def chop_trajectory(
    filenames, filename_topology="", skip_frames=1, frame_chunk=1, chunk_fix=False
):
    """Creates a generator that sequentially provides chunks of the trajectory.

    For large amounts of data or systems with low resources, this function
    conveniently creates a trajectory generator so that the frames from one or
    multiple files can be loaded a few at a time. It is also possible to skip
    frames so that only one every n-th frames are added to the trajectory
    chunks.

    The 'frame_chunk' argument allows to specify the chunk size or the number
    of frames that the generator bundles together to load in every chunk.
    However, the function handles the trajectory files sequentially and
    independently, hence, every time it arrives to the end of one of the files
    it is possible that it yields a smaller chunk than the specified size.

    Iterload's function from MDTraj's version 1.9.3 does not fully support PDB
    files, hence, MD2reflect issues a warning when attempting to load one or
    more PDB files using 'chop_trajectory'. This function allows to load them
    by chunks and even using the option to skip frames, but then the chunks
    must contain only one frame at a time.

    A bug in MDTraj's version 1.9.3 prevented loading chunks of the appropriate
    size. Although this has been solved in later versions, a fix is included
    here just in case. If the chunk size is larger than the specified one, the
    program issues a warning to activate the fix. The fix option should only be
    activated when necessary.

    Parameters:
        filenames (str or list of str): Path to the file, or list of paths in
            the case of multiple files.
        filename_topology (str): Path to a file with the topological
            information of the simulation. Is only necessary if the format of
            the trajectory file/s do/es not include this information.
        skip_frames (int): Default is one (loads all frames).
        frame_chunk (int): Default is one (loads specified frames one by one).
        chunk_fix (bool): Fixes a bug in MDtraj 1.9.3 and older versions.
            Default is False (doesn't apply the fix).

    Yields:
        chunk (trajectory generator): A generator that loads trajectory bundles
        of only a few frames at a time from the trajectory file/s.
    """
    original_frame_chunk = frame_chunk

    # Single file: Checks if the file is a pdb file.

    has_pdb = False
    if type(filenames) == str and filenames[-3:].lower() != "pdb":
        if skip_frames is not None and skip_frames > 1.0 and chunk_fix is True:
            frame_chunk = frame_chunk / skip_frames
    elif type(filenames) == str and filenames[-3:].lower() == "pdb":
        has_pdb = True
        print(
            "Some MDTraj versions have limited PDB support for iterload. "
            "Use the 'chop_trajectory' function with caution."
        )

    # Multiple files: Checks if any of the files is a pdb file.
    elif type(filenames) == list:
        for name in filenames:
            if name[-3:].lower() == "pdb":
                has_pdb = True
        if has_pdb is True:
            print(
                "Some MDTraj versions have limited PDB support for iterload."
                " Use the 'chop_trajectory' function with caution."
            )

        if (
            has_pdb is not True
            and skip_frames is not None
            and skip_frames > 1.0
            and chunk_fix is True
        ):
            frame_chunk = frame_chunk / skip_frames

    chunk_warning = (
        "\nWARNING: The requested chunk size does not correspond "
        "with the actual size of the following chunk. "
        "Activate/deactivate the 'chunk_fix' option and try "
        "again.\n"
    )

    if type(filenames) == str and filename_topology == "":
        for chunk in md.iterload(filenames, stride=skip_frames, chunk=frame_chunk):
            if len(chunk) > original_frame_chunk and has_pdb is False:
                print(chunk_warning)
            yield chunk
    elif type(filenames) == str and filename_topology != "":
        for chunk in md.iterload(
            filenames, top=filename_topology, stride=skip_frames, chunk=frame_chunk
        ):
            if len(chunk) > original_frame_chunk and has_pdb is False:
                print(chunk_warning)
            yield chunk

    elif type(filenames) == list and filename_topology == "":
        for file in filenames:
            for chunk in md.iterload(file, stride=skip_frames, chunk=frame_chunk):
                if len(chunk) > original_frame_chunk and has_pdb is False:
                    print(chunk_warning)
                yield chunk
    else:
        for file in filenames:
            for chunk in md.iterload(
                file, top=filename_topology, stride=skip_frames, chunk=frame_chunk
            ):
                if len(chunk) > original_frame_chunk and has_pdb is False:
                    print(chunk_warning)
                yield chunk


def get_trajectory_chunk(trajectory_chunks_generator):
    """Returns one chunk of the trajectory chunks generator.

    This function takes the trajectory generator yielded by 'chop_trajectory'
    and returns one of the chunks or trajectories of only a few frames.

    Returns:
        chunk (trajectory): Sample trajectory object yielded by the chunk
        generator.
    """
    for chunk in trajectory_chunks_generator:
        return chunk
