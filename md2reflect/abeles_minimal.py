import warnings
import numpy as np

from scipy.interpolate import interp1d

warnings.filterwarnings("ignore", category=RuntimeWarning)


def abeles_reflectivity(q, sld, sld_abs, thickness, roughness):
    """This function should be defined, or fix the name at "prepare_R_convolution"."""
    pass


class Error(Exception):
    pass


"""
Reflectivity simulation tool.

# Here some examples of the main functions
from matplotlib import pyplot as plt
q = np.linspace(0,.5,1000)

# simple reflectivity, which uses pure Fresnel
count = 1
sld = np.array([0,2e-6, 3e-6])
roughness = np.array([0,0])
thickness = np.array([0, 100, np.inf])
plt.semilogy(q,  theoretical_reflectivity(q,
                                        sld,
                                        sld*0,
                                        thickness,
                                        roughness),
       lw = 2,
       label = str(count)
       )

# simple reflectivity, which uses Ndevo-Croce
count += 1
sld = np.array([0,2e-6, 3e-6])
roughness = np.array([2,3])
thickness = np.array([0, 100, np.inf])
plt.semilogy(q,  theoretical_reflectivity(q,
                                        sld,
                                        sld*0,
                                        thickness,
                                        roughness),
       lw = 2,
       label = str(count)
       )

# resolution effect at a tof instrument with 3% realtive q resol
count += 1
sld = np.array([0,2e-6, 3e-6])
roughness = np.array([2,3])
thickness = np.array([0, 100, np.inf])
plt.semilogy(q,  simul_reflect_meas_cst_dq_q(q,
                                           sld,
                                           sld*0,
                                           thickness,
                                           roughness,
                                           ),
       lw = 2,
       label = str(count)
       )

# resolution effect at a monochromatic instrument
count += 1
sld = np.array([0,2e-6, 3e-6])
roughness = np.array([2,3])
thickness = np.array([0, 100, np.inf])
plt.semilogy(q,  simul_reflect_meas_monochr(q,
                                          sld,
                                          sld*0,
                                          thickness,
                                          roughness,
                                          wl = 1,
                                          dwl=.043
                                           ),
       lw = 2,
       label = str(count)
       )

plt.legend()

"""

TWOPI = 2 * np.pi
FWHM2SIGMA = 1 / (2 * np.sqrt(2 * np.log(2)))
INVSQRT2PI = 1 / np.sqrt((np.pi * 2))


def simul_reflect_meas_cst_dq_q(
    q,
    sld,
    sld_abs,
    thickness,
    roughness,
    bckg=0,
    dq_q=0.03,
    resol_fn="gauss",
    npts_res=201,
    nsigma=5,
    normalize=True,
    verbose=False,
):

    """
    Simulate a reflectivity curve including resolution effects assuming a
    constant dq /q. This is the case at refsans

    !!! this is a high level function  which recomputes the resolution
    function at each call. -> if the resolution does not need to be a fit
    parameter, this repeated evaluation is a waste of time

    """

    # sanity check, due to its highest value at q == zero (most often), it is
    # best to enforce this
    if np.mod(npts_res, 2) == 0:
        raise ValueError("npts_res must be an odd number !")

    # evaluate the resolution function
    # (q_target, fres) = resol_fn(q, dq_q=dq_q, npts=npts_res, nsigma=nsigma)
    # t0 = time.time()
    (q_target, fres) = get_resolution_function(
        q,
        q_resol_charact=dq_q,
        npts=npts_res,
        nsigma=nsigma,
        fn=resol_fn,
        normalize=normalize,
        verbose=verbose,
    )

    R_target = prepare_R_convolution_fast(
        q, q_target, sld, sld_abs, thickness, roughness
    )
    # t2 = time.time()
    R_target += bckg
    # t3 = time.time()
    # apply the resolution function
    Rc = apply_resol_fn(R_target, fres)
    # t4 = time.time()
    # print("calc resol_fn", t1 - t0)
    # print("prepare_conv", t2 - t1)
    # print("add_bckg", t3 - t2)
    # print("convolution", t4 - t3)
    return Rc


def simul_reflect_meas_monochr(
    q,
    sld,
    sld_abs,
    thickness,
    roughness,
    bckg=0,
    wl=1.5418,
    dwl=0.01,
    dtheta=0.001,
    npts_res=201,
    nsigma=5,
):

    """
    Simulate a reflectivity curve including resolution effects
    (monochromatic instrument)
    """

    #

    # sanity check, due to its highest value at q == zero (most often), it is
    # best to enforce this
    if np.mod(npts_res, 2) == 0:
        raise ValueError("npts_res must be an odd number !")
    # evaluate the resolution function
    (q_target, fres) = get_resol_fn_monochr(
        q, wl=wl, dwl=dwl, dtheta=dtheta, npts=npts_res, nsigma=nsigma
    )
    # calculate the reflectivity at the target points needed for convolution
    R_target = prepare_R_convolution_fast(
        q, q_target, sld, sld_abs, thickness, roughness
    )
    R_target += bckg
    # apply the resolution function
    Rc = apply_resol_fn(R_target, fres)

    return Rc


# utility functions (for resolution convolution)
def get_resol_fn_monochr(q_meas, wl, dwl, dtheta, npts=200, nsigma=5, verbose=False):

    """
    Calculate the resolution function and the points where it needs to be
    evaluated to convolve with the data.
    This is for the case where dtheta=cst and dwl = cst
    (i.e: monochromatic instrument)

    input:

        q_meas    :    the array of experimentally measured (or simulated) q
        wl        :    the wavelength is Ang.
        dwl       :    the absolute uncertainty on wl (Ang)
        dtheta    :    the beam divergence (deg)
        npts      :    number of points for the evaluation of convolution
        nsigma    :    how many sigma to consider for convolution

    output

        a tuple (q_target, fres) where

        q_target  :    the q positions where the reflectivity curve needs to
                       be evaluated for convolution
        fres      :    the resolution function at q_target (a vector
                       of npts length)

    """

    qtarget = np.empty((len(q_meas), npts))
    theta = np.arcsin(wl * q_meas / (4 * np.pi))
    dq = q_meas * np.sqrt((np.radians(dtheta) / theta) ** 2 + (dwl / wl) ** 2)
    delta = nsigma * dq
    qmin = q_meas - delta
    qmax = q_meas + delta
    for count, th in enumerate(theta):
        # where do we need to evaluate the function for this particular angle
        q_integration = np.linspace(qmin[count], qmax[count], npts)

        qtarget[count] = q_integration[:]

    # the resolution function for each step is always the same, just with
    # q rescaled this is reflected in q_target, so that we can calculate
    # fres once and use a simple normal distribution form
    x = np.linspace(-nsigma, nsigma, npts)
    dx = 1.0 * nsigma * 2 / npts
    fres = gauss(x) * dx
    if verbose:
        print("integral of the gaussian in abeles.resol_fn", dx, np.nansum(fres))
    return (qtarget, fres)


def get_resolution_function(
    q_meas,
    q_resol_charact,
    npts=2001,
    nsigma=5,
    fn="gauss",
    safe_eval_thresh=0.99,
    normalize=True,
    verbose=False,
):

    """
    Obtain the resolution function as a tupple (q, resol_fn(q)) which can then
    be used to convolve with the data.

    Given the measured q positions and the characteristics of the resolution
    function, one needs to evaluate the value of the resol_fn at intermediate
    positions in order to perform the convolution correctly. This function
    finds the intermediate position and computes resol_fn

    This function accepts several types of resolution functions (see below).
    All of them are first supposed to be approximately of the type
    dq/q = constant. This approach is used to define the domain over which
    evaluation is needed in order to do a proper convolution.
    Once the target domain is known, the exact resolution fucnction is
    evaluated. It is thus clear that a conservative (i.e slightly large dq_q)
    needs to be chosen in the first step in order not to miss the tails.

    input:

        q_meas          :  the array of measured or simulated q
        q_resol_charact :  an approximation of dq_q.
                           In the case of a gaussian resolution function, this
                           value is exactly dq_q.
                           This value needs to be large enough to be sure not
                           to miss the tails of the actual resolution function.
                           This parameter has to be chosen in conjonction with
                           nsigma (see below)

        npts            :  number of points for the evaluation of convolution,
                           it defines somehow the resolution on the resolution
                           function. An odd number of points is enforced since
                           the resolution function is assumed to peak at its
                           center

        nsigma           : how many sigma to consider for convolution, that
                           is how ar from q_meas should one extend the
                           convolution

        fn               : the actual function to be used to compute the
                           resolution. Different options are available:
                       * specify the fn by name: "gauss" is the only one
                         implemented (fname == "gauss", a string!)
                       * provide a single function object for computing
                         resol(q). This is analogous to "gauss" but for
                         arbitrary functions of q
                       * provide a list (or any iterable) of function objects,
                         this iterable must have one entry per q_meas value
                         (fn is a list of fn). This is useful for instance when
                         the beam is described by two components of variable
                         intensity or if an interpolator for the actual beam
                         distribution is used as resolution function.

    output:

        a tuple (q_target, fres) where:

        q_target  :    the q positions where the reflectivity curve needs to
                       be evaluated for convolution
        fres      :    the resolution function at q_target (a vector of
                       npts length)

    """

    # TODO #FIXME npts should be evaluated differently for rapidly oscillating fns....
    # would a pseudo continuous integration be better (and not too slow?)
    # there potential is redundancy in the target qs

    # sanity check, due to its symmetry and highest value at
    # q == zero (most often), it is  best to enforce this
    if np.mod(npts, 2) == 0:
        raise ValueError("npts must be an odd number !")

    npts = int(npts)
    linsp = np.linspace(0, 1, npts)

    if isinstance(q_meas, np.float64):
        q_meas = np.array([q_meas])
    qtarget = np.empty((len(q_meas), npts))

    # for each qmeas we need to calculate f at the qtarget positions
    for count, qm in enumerate(q_meas):
        # the width of the resol_fn is proportional to q
        sigma = q_resol_charact * abs(qm)

        # where do we need to evaluate the function for this particular qmeas
        delta = nsigma * sigma
        qmin = qm - delta
        #        qmax = qm + delta

        # calling linspace in a loop costs too much time
        # using this rescaling is much faster!
        # WAS qtarget[count] =n p.linspace(qmin, qmax, npts)
        qtarget[count] = linsp * 2 * delta + qmin

    xxg = np.linspace(-nsigma, nsigma, npts)
    dxxg = xxg[1] - xxg[0]

    xx = np.linspace(-nsigma * q_resol_charact, nsigma * q_resol_charact, npts)
    dxx = xx[1] - xx[0]

    if fn == "gauss":

        # fn type is given by name (gauss)
        # here we assume the parameters choice is correct and
        # we do not adjust the integral
        weight = gauss(xxg) * dxxg
        # optionaly test is we cover enough of the deomain of resol_fn
        if safe_eval_thresh is not None:
            check_resol_fn_integral(weight, safe_eval_thresh, verbose=verbose)

    elif isinstance(fn, (list, tuple, np.ndarray)):
        # FIXME check if shape is valid
        # an iterable containing function objects was passed
        # there is one function per qmeas and each one needs to be calculated
        # at the corresponding qtargets
        weight = []
        for count, f in enumerate(fn):
            try:
                this_weight = f(xx) * dxx
                if normalize:
                    # if the resolution function is based on an interpolation
                    # of a real primary beam, it can be pretty noisy and the
                    # integral will not be 1 (errors in the order of 4-5%
                    # are typical)
                    # it might be a better idea to renormalize the
                    # integral to 1.
                    # the effect of those errors is stonger where the
                    # reflectivity is high and one sees it very well
                    # at the total  reflection.
                    this_weight /= this_weight.sum()
                if safe_eval_thresh is not None:
                    check_resol_fn_integral(
                        this_weight, safe_eval_thresh, verbose=verbose
                    )
            except TypeError:
                this_weight = np.nan * np.ones_like(xx)
            weight.append(this_weight)
        weight = np.array(weight)
    else:
        # FIXME  only a single fn object was passed
        for count, f in enumerate(fn):
            weight = f(xx) * dxx
            weight /= weight.sum()
            if safe_eval_thresh is not None:
                check_resol_fn_integral(weight, safe_eval_thresh, verbose=verbose)

    return (qtarget, weight)


def check_resol_fn_integral(fresdx, thresh=0.99, verbose=False):

    """
    check that the integral of the resolution function over the provided range
    approaches 1
    """

    integral = np.nansum(fresdx)
    if verbose:
        print("integral of the resolution fn in abeles.resol_fn", integral)
    if not integral >= thresh:
        msg = "npts_res is too small and leads to integral(fres) ="
        msg += str(integral)
        raise ValueError(msg)


def prepare_R_convolution(q_target, sld, sld_abs, thickness, roughness):

    """
    given the qtarget calculated by get_resolution_function and
    structural parameters, calculate the reflectivity where needed for
    convolution
    """

    R = np.empty((len(q_target), len(q_target[0])))
    r = np.empty(len(q_target[0]))
    #    for count in xrange(len(q_target)):
    #        for i in xrange(len(q_target[count])):
    for count in range(len(q_target)):
        for i in range(len(q_target[count])):
            r[i] = (
                abs(
                    abeles_reflectivity(
                        q_target[count][i], sld, sld_abs, thickness, roughness
                    )
                )
                ** 2
            )
        R[count] = r[:]
    return R


# wrapper for reflectivity, excluding resolution effects
def theoretical_reflectivity(q, sld, sld_abs, thickness, roughness):

    """
    reflectivity calculation without resolution nor bckg
    """

    if (roughness == 0).all():
        # The pure Fresnel approach is valid!
        R = abs(abeles_fresnel_reflectivity(q, sld, sld_abs, thickness)) ** 2
    else:
        # We have to use Nevo-Croce
        R = (
            abs(abeles_nevo_croce_reflectivity(q, sld, sld_abs, thickness, roughness))
            ** 2
        )
    return R


# theoretical reflectivity, pure Fresnel case
def abeles_fresnel_reflectivity(qz, sld, sld_abs, thickness):

    """
    Simulate a reflectivity curve via the Abeles algorithm
    """

    kz = qz * 0.5
    kz = kz.astype(np.complex128)

    drho = 4 * np.pi * (sld - sld[0])
    drho_abs = 4 * np.pi * (sld_abs - sld_abs[0])

    r = np.empty(len(sld), dtype=np.complex128)

    reflectivity = np.empty(len(kz), dtype=np.complex128)

    M = np.empty((2, 2), dtype=np.complex128)
    M_temp = np.empty((2, 2), dtype=np.complex128)

    # number of layer excluding atm and subst
    nl = len(sld) - 2

    # phase factor arrays
    ei2qd = np.empty(len(sld), dtype=np.complex128)
    e_i2qd = np.empty(len(sld), dtype=np.complex128)

    for kcount in range(len(kz)):

        kz2 = kz[kcount] ** 2
        # momentum in substrate
        pmp = np.sqrt(kz2 - (drho[nl + 1] + drho_abs[nl + 1] * 1j))
        # we go layers backwards up to the atm
        for layer in range(nl, -1, -1):

            pm = np.sqrt(kz2 - (drho[layer] + drho_abs[layer] * 1j))
            r[layer + 1] = (pm - pmp) / (pm + pmp)

            opt_path = pm * thickness[layer]

            ei2qd[layer] = np.exp(1j * opt_path)
            e_i2qd[layer] = 1 / ei2qd[layer]
            pmp = pm

        # prepare the transfer matrix and a temp for multiplication,
        M[0, 0] = 1
        M[0, 1] = r[1]
        M[1, 0] = r[1]
        M[1, 1] = 1

        M_temp = M.copy()

        if nl > 0:
            for layer in range(1, nl + 1):
                M_temp[0, 0] = (
                    M[0, 0] * ei2qd[layer] + M[0, 1] * r[layer + 1] * e_i2qd[layer]
                )
                M_temp[0, 1] = (
                    M[0, 0] * r[layer + 1] * ei2qd[layer] + M[0, 1] * e_i2qd[layer]
                )
                M_temp[1, 0] = (
                    M[1, 0] * ei2qd[layer] + M[1, 1] * r[layer + 1] * e_i2qd[layer]
                )
                M_temp[1, 1] = (
                    M[1, 0] * r[layer + 1] * ei2qd[layer] + M[1, 1] * e_i2qd[layer]
                )

                M = M_temp.copy()
        reflectivity[kcount] = M[1, 0] / M[0, 0]
    return reflectivity


# theoretical reflectivity, general case
def abeles_nevo_croce_reflectivity(qz, sld, sld_abs, thickness, roughness):

    """
    Simulate a reflectivity curve via the Abeles algorithm
    """

    kz = qz * 0.5
    kz = kz.astype(np.complex128)

    drho = 4 * np.pi * (sld - sld[0])
    drho_abs = 4 * np.pi * (sld_abs - sld_abs[0])

    r = np.empty(len(sld), dtype=np.complex128)

    reflectivity = np.empty(len(kz), dtype=np.complex128)

    M = np.empty((2, 2), dtype=np.complex128)
    M_temp = np.empty((2, 2), dtype=np.complex128)

    roughness2 = roughness ** 2

    # number of layer excluding atm and subst
    nl = len(sld) - 2

    # phase factor arrays
    ei2qd = np.empty(len(sld), dtype=np.complex128)
    e_i2qd = np.empty(len(sld), dtype=np.complex128)

    for kcount in range(len(kz)):

        kz2 = kz[kcount] ** 2
        # momentum in substrate
        pmp = np.sqrt(kz2 - (drho[nl + 1] + drho_abs[nl + 1] * 1j))
        # we go layers backwards up to the atm
        for layer in range(nl, -1, -1):

            pm = np.sqrt(kz2 - (drho[layer] + drho_abs[layer] * 1j))
            r[layer + 1] = (pm - pmp) / (pm + pmp)
            # the index for roughness is one less...
            r[layer + 1] *= np.exp(-2 * pm * pmp * roughness2[layer])  #

            opt_path = pm * thickness[layer]

            ei2qd[layer] = np.exp(1j * opt_path)
            e_i2qd[layer] = 1 / ei2qd[layer]
            pmp = pm

        # prepare the transfer matrix and a temp for multiplication,
        M[0, 0] = 1
        M[0, 1] = r[1]
        M[1, 0] = r[1]
        M[1, 1] = 1

        M_temp = M.copy()

        if nl > 0:
            for layer in range(1, nl + 1):
                M_temp[0, 0] = (
                    M[0, 0] * ei2qd[layer] + M[0, 1] * r[layer + 1] * e_i2qd[layer]
                )
                M_temp[0, 1] = (
                    M[0, 0] * r[layer + 1] * ei2qd[layer] + M[0, 1] * e_i2qd[layer]
                )
                M_temp[1, 0] = (
                    M[1, 0] * ei2qd[layer] + M[1, 1] * r[layer + 1] * e_i2qd[layer]
                )
                M_temp[1, 1] = (
                    M[1, 0] * r[layer + 1] * ei2qd[layer] + M[1, 1] * e_i2qd[layer]
                )

                M = M_temp.copy()
        reflectivity[kcount] = M[1, 0] / M[0, 0]
    return reflectivity


# Utility functions for convolution


def prepare_fn_convolution_fast(fn, q_meas, q_target, *args, **kwargs):

    """
    Compute needed data to evaluate a function of q as affected by resolution.

    In order to evaluate the data at q_meas (the values where we want to
    compare experimental data to our simulation), we must also compute
    the data at auxiliary points which contribute to the measured intensity
    due to the effect of the resolution function.

    given the q_target calculated by resol_fn and other args, kwargs, calculate
    the fn where needed for convolution.

    input:

        fn        :    a function object accepting (q, *args, **kwargs)
        q_meas    :    q values at which the fn is experimentally availble
        q_target  :    q values at which the convoluted fn is needed

    output:

        fn(q_target, *args, **kwargs)

    """

    # we calculate the reflectivity at the q_meas
    # we will need to have values a qmin (1-dq_q) and qmax (1+dq_q)
    # as well (for the resol_fn)
    qq = np.empty(len(q_meas) + 2)
    qq[1:-1] = q_meas
    # first q_target are points for the beginning of the curve,
    # i.e first point is qmin (1-dq_q)
    qq[0] = q_target[0][0]
    # last q_target are points for the end of the curve,
    # i.e last point is qmax (1+dq_q)
    qq[-1] = q_target[-1][-1]
    Fn = fn(qq, *args, **kwargs)
    # now interpolate at target points (which are needed for application of
    # the resolution function
    interpolator = interp1d(qq, Fn, bounds_error=False)
    Fn_target = interpolator(q_target)
    return Fn_target


def prepare_R_convolution_fast(q_meas, q_target, sld, sld_abs, thickness, roughness):

    """
    Compute needed data to evaluate the reflectivity as affected by resolution.

    In order to evaluate the data at q_meas (the values where we want to
    compare experimental data to our simulation), we must also compute
    the data at auxiliary points which contribute to the measured intensity
    due to the effect of the resolution function.

    given the q_target calculated by resol_fn and structural parameters, calculate
    the reflectivity where needed for convolution. The reflectivity curve is
    evaluated via Abeles at the q_meas and interpolation is used elsewhere
    (hence the "fastness").

    """

    # we calculate the reflectivity at the q_meas
    # we will need to have values a qmin (1-dq_q) and qmax (1+dq_q)
    # as well (for the resol_fn)
    qq = np.empty(len(q_meas) + 2)
    qq[1:-1] = q_meas
    # first q_target are points for the beginning of the curve,
    # i.e first point is qmin (1-dq_q)
    qq[0] = q_target[0][0]
    # last q_target are points for the end of the curve,
    # i.e last point is qmax (1+dq_q)
    qq[-1] = q_target[-1][-1]
    # R = theoretical_reflectivity(qq, sld, sld_abs, thickness, roughness)
    R = theoretical_reflectivity(qq, sld, sld_abs, thickness, roughness)
    # now interpolate at target points (which are needed for application of
    # the resolution function
    # this is left as doc, Numpy or own interp are **exactly** same speed
    # using numpy
    interpolator = interp1d(qq, np.log10(R), bounds_error=False, assume_sorted=True)
    R_target = 10 ** interpolator(q_target)
    # using simpler linear interpolation would be exactly as fast
    #    R_target = 10*__*interp( qq, np.log10(R), q_target)
    return R_target


def __interp(x, y, target):

    dx = np.diff(x)
    dy = np.diff(y)
    slope = dy / dx
    index = np.digitize(target, x) - 1
    # taking care of last target
    # which sits at edge of input data
    index[-1] = -1
    yt = y[index] + slope[index] * (target - x[index])

    return yt


def apply_resol_fn(R, resol_fn):

    """
    given a resolution function array as from resol_fn and a reflectivity array
    as from prepare_R_convolution, apply the resolution function
    """
    if resol_fn.ndim == 1:
        res = np.dot(resol_fn, R.T)
    elif resol_fn.ndim == 2:
        res = np.empty(len(resol_fn))
        for count, (fn, r) in enumerate(zip(resol_fn, R)):
            res[count] = np.dot(fn, r.T)

    # was initially done "by hand"
    # res = np.empty(len(R))
    #
    # for count in xrange(len(R)):
    #     # BEWARE ! using convolve in this case
    #     # the edge effects are absolutely dramatic!!! so use only valid points
    #     this_res = np.convolve(R[count], resol_fn, 'valid')
    #     this_res /= len(this_res)
    #     res[count] = this_res.sum()
    return res


_prefact = 1 / np.sqrt(np.pi * 2)


def gauss(x):
    res = _prefact * np.exp(-(x ** 2) * 0.5)
    return res


if __name__ == "__main__":
    from matplotlib import pyplot as plt

    q = np.linspace(0, 0.5, 1000)

    # simple reflectivity, which uses pure Fresnel
    count = 1
    sld = np.array([0, 2e-6, 3e-6])
    roughness = np.array([0, 0])
    thickness = np.array([0, 100, np.inf])
    plt.semilogy(
        q,
        theoretical_reflectivity(q, sld, sld * 0, thickness, roughness),
        lw=2,
        label=str(count),
    )

    # simple reflectivity, which uses Ndevo-Croce
    count += 1
    sld = np.array([0, 2e-6, 3e-6])
    roughness = np.array([2, 3])
    thickness = np.array([0, 100, np.inf])
    plt.semilogy(
        q,
        theoretical_reflectivity(q, sld, sld * 0, thickness, roughness),
        lw=2,
        label=str(count),
    )

    # resolution effect at a tof instrument with 3% realtive q resol
    count += 1
    sld = np.array([0, 2e-6, 3e-6])
    roughness = np.array([2, 3])
    thickness = np.array([0, 100, np.inf])
    plt.semilogy(
        q,
        simul_reflect_meas_cst_dq_q(
            q,
            sld,
            sld * 0,
            thickness,
            roughness,
        ),
        lw=2,
        label=str(count),
    )

    # resolution effect at a monochromatic instrument
    count += 1
    sld = np.array([0, 2e-6, 3e-6])
    roughness = np.array([2, 3])
    thickness = np.array([0, 100, np.inf])
    plt.semilogy(
        q,
        simul_reflect_meas_monochr(
            q, sld, sld * 0, thickness, roughness, wl=1, dwl=0.043
        ),
        lw=2,
        label=str(count),
    )

    plt.legend()
