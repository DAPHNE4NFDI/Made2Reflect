import copy
import periodictable


def get_elements(traj):
    """Determine how many elements there are in the trajectory and their type.

    Parameters:
        traj (trajectory): The loaded simulation trajectory.

    Returns:
        different_elements (list of tuples): A list of the elements which are
        present in the trajectory.

    Example:

    ::

        get_elements(traj)

        >>> [(1 'hydrogen', 'H', 1.007947, 0.12),
            (6, 'carbon', 'C', 12.01078, 0.17),
            (7, 'nitrogen', 'N', 14.00672, 0.155),
            (29, 'copper', 'Cu', 63.5463, 0.14),
            (35, 'bromine', 'Br', 79.9041, 0.185)]
    """
    different_elements = sorted((set([a.element for a in traj.topology.atoms])))
    return different_elements


def find_atom_types(traj):
    """Lists the molecules and the different types of atoms in each molecule.

    This function finds the molecules or individual atoms in the simulation and
    lists the labels of their corresponding atoms.

    Conveniently, the substrate layer is normally also detected as an
    individual atom type or molecule.

    Parameters:
        traj (trajectory): The loaded simulation trajectory.

    Returns:
        list_atom_types (dict of list): Dictionary of the molecules present in
        the trajectory with a list of the labels for the atoms types present in
        each molecule.

    Example:

    ::

        find_atom_types(traj)

        >>> {'CU': ['CU'],
             'CU1': ['CU1'],
             'bmim': ['C1', 'C10', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'H',
                      'H11', 'H12', 'H13', 'H14', 'H15', 'H16', 'H17', 'H18',
                      'H19', 'H20', 'H21', 'H22', 'H23', 'H24', 'N8', 'N9'],
             'BR': ['BR']}

        # The first two entries are the electrodes at each side of the
        # simulation.
    """
    # Find different molecules or single atoms
    list_atom_types = {residue.name: [] for residue in traj.topology.residues}

    # Find atom types for each molecule
    for selected_residue in list_atom_types:
        (list_atom_types[selected_residue]) = sorted(
            set(
                atom.name
                for atom in traj.topology.atoms
                if (atom.residue.name == selected_residue)
            )
        )

    return list_atom_types


def find_atom_types_elements(traj):
    """Lists the molecules, their different types of atoms, and its elements.

    This function finds the molecules or individual atoms in the simulation,
    and then makes a dictionary for each molecule where the element of each
    atom type is specified.

    Conveniently, the substrate layer is normally also detected as an
    individual atom type or molecule.

    Parameters:
        traj (trajectory): The loaded simulation trajectory.

    Returns:
        list_atom_types_elements (dict of dict): Dictionary of the molecules
        present in the trajectory, each with a dictionary of the elements that
        correspond to the atom types present in the molecule.

    Example:
    ::

        find_atom_types_elements(traj)

        >>> {'CU': {'CU': 'Cu'},
             'CU1': {'CU1': 'Cu'},
             'bmim': {'C1': 'C', 'C10': 'C', 'C2': 'C', 'C3': 'C', 'C4': 'C',
             'C5': 'C', 'C6': 'C', 'C7': 'C', 'H': 'H', 'H11': 'H', 'H12': 'H',
             'H13': 'H', 'H14': 'H', 'H15': 'H', 'H16': 'H', 'H17': 'H',
             'H18': 'H', 'H19': 'H', 'H20': 'H', 'H21': 'H', 'H22': 'H',
             'H23': 'H', 'H24': 'H', 'N8': 'N', 'N9': 'N'},
             'BR': {'BR': 'Br'}}

        # The first two entries are the electrodes at each side of the
        # simulation.
    """
    # Find different molecules or single atoms
    list_atom_types_elements = {residue.name: [] for residue in traj.topology.residues}

    # Find atom types for each molecule
    for selected_residue in list_atom_types_elements:
        (list_atom_types_elements[selected_residue]) = {
            atom.name: atom.element.symbol
            for atom in traj.topology.atoms
            if (atom.residue.name == selected_residue)
        }
        (list_atom_types_elements[selected_residue]) = {
            atom_type: list_atom_types_elements[selected_residue][atom_type]
            for atom_type in sorted(list_atom_types_elements[selected_residue])
        }

    return list_atom_types_elements


def isotopic_substitution(traj, deuterated):
    """Substitutes the atoms in the list of atoms by all the indicated
    isotopes.

    This function allows to carry out isotopic substitution to any atom type in
    the simulation.

    A list of the deuterated atoms must be constructed where for each atom it
    must be specified the molecule they belong to, their label, and the desired
    substitution isotope.

    With this list of isotopes to substitute, a modified dictionary of atoms in
    the trajectory is generated.

    Parameters:
        traj (trajectory): The loaded simulation trajectory.
        deuterated (TODO): List of atoms to substitute. Every atom needs three
            things, the molecule they belong to, their label, and the isotope
            they should be substituted by. Isotopes must be indicated using
            their atomic mass number (or number of nucleons), a dash, and their
            element symbol, such as "37-Cl" or "15-N". When no number is
            present, this refers to the natural abundance of the element.
            Hydrogen isotopes can also be specified by their name, such as "D"
            for deuterium or "T" for tritium.

    Returns:
        list_atom_types_elements (dict of dict): Dictionary of the molecules
        present in the trajectory, each with a dictionary of the isotopes that
        correspond to the atom types present in each molecule.

    Example:
    ::

        deuterated = [("bmim", "H20", "D"), ("bmim", "H11", "D"),
                      ("bmim", "H14", "D"), ("bmim", "H15", "D"),
                      ("bmim", "H16", "D"), ("bmim", "H17", "D")]

        isotopic_substitution(traj, deuterated)

        >>> {'CU': {'CU': 'Cu'},
             'CU1': {'CU1': 'Cu'},
             'bmim': {'C1': 'C', 'C10': 'C', 'C2': 'C', 'C3': 'C',
                      'C4': 'C', 'C5': 'C', 'C6': 'C', 'C7': 'C',
                      'H': 'H', 'H11': 'D', 'H12': 'H', 'H13': 'H',
                      'H14': 'D', 'H15': 'D', 'H16': 'D', 'H17': 'D',
                      'H18': 'H', 'H19': 'H', 'H20': 'D', 'H21': 'H',
                      'H22': 'H', 'H23': 'H', 'H24': 'H',
                      'N8': 'N', 'N9': 'N'},
             'BR': {'BR': 'Br'}}

        # All atoms identified as deuterium ('D') were previously hydrogen
        # ('H').
    """
    list_atom_types_elements = find_atom_types_elements(traj)
    for atom_type in deuterated:
        if list_atom_types_elements[atom_type[0]][atom_type[1]]:
            list_atom_types_elements[atom_type[0]][atom_type[1]] = atom_type[2]
    return list_atom_types_elements


def get_elements_bc_neutrons(traj):
    """Retrieves the neutron coherent scattering lengths for each element.

    This function retrieves the neutron coherent scattering lengths (including
    their imaginary part) of all elements present in the trajectory simulation.

    It is possible to carry out isotopic substitution for all atoms of an
    element in the simulation simply by manually modifying its SLD values (see
    example below).

    Parameters:
        traj (trajectory): The loaded simulation trajectory.

    Returns:
        bc_table (dict of tuples): Dictionary of all elements present in the
        simulation with their symbol, real part of the neutron scattering
        length, and also the imaginary part of the neutron scattering length,
        which corresponds to the absorption scattering length).

    Examples:
    ::

        get_elements_bc_neutrons(traj)

        >>> {'helium': ('He', 3.26, 0.0),
             'carbon': ('C', 6.6484, 0.0),
             'nitrogen': ('N', 9.36, 0.0),}

    ::

        # Isotopic substitution of natural abundance Helium by Helium-3

        bc_table = get_elements_bc_neutrons(traj)

        bc_table['helium'] = ('He',
                      periodictable.elements.isotope("3-He").neutron.b_c,
                      periodictable.elements.isotope("3-He").neutron.b_c_i)

        >>> bc_table = {'helium': ('He', 5.74, -1.48),
                        'carbon': ('C', 6.6484, 0.0),
                        'nitrogen': ('N', 9.36, 0.0),}
    """
    different_elements = get_elements(traj)

    bc_table = {}

    for index_element in range(len(different_elements)):
        element_name = str(different_elements[index_element].name)
        element_symbol = str(different_elements[index_element].symbol)
        select_periodictable = getattr(periodictable, element_name)
        element_bc = select_periodictable.neutron.b_c
        element_bci = select_periodictable.neutron.b_c_i
        if element_bci is None:
            element_bci = 0.0

        # in fm
        bc_table[element_name] = (element_symbol, element_bc, element_bci)

    return bc_table


def get_atom_types_bc_neutrons(list_atom_types_elements):
    """Retrieves the neutron coherent scattering lengths of each atom type.

    This function retrieves the neutron coherent scattering lengths (including
    their imaginary part) of all atom types present in the dictionary of
    molecules with simulation.

    Parameters:
        list_atom_types_elements (dict of dicts): Dictionary of the molecules
            present in the trajectory, each with a dictionary of the atom types
            in that molecule, and where each atom type has its element symbol
            or isotope.

    Returns:
        bc_table (dict of dicts): Dictionary of the molecules
        present in the trajectory, each with a dictionary of the atom types in
        that molecule, and where each atom type has a tuple with its element
        symbol or isotope, the real part of the neutron coherent scattering
        length, and the imaginary part of the neutron coherent scattering
        length (which corresponds to the absorption scattering length).

    Example:
    ::

        get_atom_types_bc_neutrons(find_atom_types_elements(traj))

        >>> {'CU': {'CU': ('Cu', 7.718, 0.0)},
             'CU1': {'CU1': ('Cu', 7.718, 0.0)},
             'bmim': {'C1': ('C', 6.6484, 0.0), 'C10': ('C', 6.6484, 0.0),
                      'C2': ('C', 6.6484, 0.0), 'C3': ('C', 6.6484, 0.0),
                      'C4': ('C', 6.6484, 0.0), 'C5': ('C', 6.6484, 0.0),
                      'C6': ('C', 6.6484, 0.0), 'C7': ('C', 6.6484, 0.0),
                      'H': ('H', -3.7409, 0.0), 'H11': ('H', -3.7409, 0.0),
                      'H12': ('H', -3.7409, 0.0), 'H13': ('H', -3.7409, 0.0),
                      'H14': ('H', -3.7409, 0.0), 'H15': ('H', -3.7409, 0.0),
                      'H16': ('H', -3.7409, 0.0), 'H17': ('H', -3.7409, 0.0),
                      'H18': ('H', -3.7409, 0.0), 'H19': ('H', -3.7409, 0.0),
                      'H20': ('H', -3.7409, 0.0), 'H21': ('H', -3.7409, 0.0),
                      'H22': ('H', -3.7409, 0.0), 'H23': ('H', -3.7409, 0.0),
                      'H24': ('H', -3.7409, 0.0), 'N8': ('N', 9.36, 0.0),
                      'N9': ('N', 9.36, 0.0)},
             'BR': {'BR': ('Br', 6.79, 0.0)}}
    """
    bc_table = copy.deepcopy(list_atom_types_elements)

    for residue in list_atom_types_elements:
        for atom_type in list_atom_types_elements[residue]:
            atom_isotope = str(list_atom_types_elements[residue][atom_type])
            atom_bc = periodictable.elements.isotope(atom_isotope).neutron.b_c
            atom_bci = periodictable.elements.isotope(atom_isotope).neutron.b_c_i
            if atom_bci is None:
                atom_bci = 0.0

            bc_table[residue][atom_type] = (atom_isotope, atom_bc, atom_bci)

    return bc_table


# xrays_energy=8.042 keV
# b_xray_scattering, b_xray_absorption


def get_elements_b_xrays(traj, xray_energy=8):
    """Retrieves the relevant x-ray scattering lengths for each element.

    This function retrieves the x-ray scattering lengths (including their
    imaginary part) of all elements present in the trajectory simulation.

    X-ray scattering lengths of materials are dependent on the energy of the
    incident x-rays. If none is specified, the default is 8 keV, which
    corresponds to copper, a common element for building x-ray sources.

    Parameters:
        traj (trajectory): The loaded simulation trajectory.
        xray_energy (float): The energy of the incident x-rays in keV. Default
            value is 8 keV, which corresponds approximately to a copper source.

    Returns:
        bc_table (dict of tuples): Dictionary of all elements present in the
        simulation with their symbol, real part of the x-ray scattering length,
        and also the imaginary part of the x-ray scattering length, which
        corresponds to the absorption scattering length).

    Example:
    ::

        get_elements_b_xrays(traj)

        >>> {'hydrogen': ('H', 2.8178952023553694, -3.1769218821552078e-06),
             'carbon': ('C', 16.96228410618459, -0.0274194449304488),
             'nitrogen': ('N', 19.81820207007492, -0.05242049632206424),
             'copper': ('Cu', 76.27551703901572, -1.7315382747620958),
             'bromine': ('Br', 96.75473595783109, -3.782638552092838)}
    """
    different_elements = get_elements(traj)

    bc_table = {}

    e_radius = periodictable.constants.electron_radius

    for index_element in range(len(different_elements)):
        element_name = str(different_elements[index_element].name)
        element_symbol = str(different_elements[index_element].symbol)
        select_periodictable = getattr(periodictable, element_name)
        f1, f2 = select_periodictable.xray.scattering_factors(energy=xray_energy)

        element_b = e_radius * f1 * 10 ** 15  # in fm
        element_bi = -e_radius * f2 * 10 ** 15  # in fm

        # in fm
        bc_table[element_name] = (element_symbol, element_b, element_bi)
    return bc_table


# default xray_energy  8.042 keV


def get_atom_types_b_xrays(list_atom_types_elements, xray_energy=8):
    """Retrieves the relevant x-ray scattering lengths for each atom type.

    This function retrieves the x-ray scattering lengths (including their
    imaginary part) of all atom types present in the trajectory simulation.

    X-ray scattering lengths of materials are dependent on the energy of the
    incident x-rays. If none is specified, the default is 8 keV, which
    corresponds to copper, a common element for building x-ray sources.

    Parameters:
        list_atom_types_elements (dict of dicts): Dictionary of the molecules
            present in the trajectory, each with a dictionary of the atom types
            in that molecule, and where each atom type has its element symbol.
        xray_energy (float): The energy of the incident x-rays in keV. Default
            value is 8 keV, which corresponds approximately to a copper source.

    Returns:
        bc_table (dict of dicts): Dictionary of the molecules present in the
        trajectory, each with a dictionary of the atom types in that molecule,
        and where each atom type has a tuple with its element symbol, the real
        part of the x-ray scattering length, and the imaginary part of the
        x-ray scattering length (which corresponds to the absorption scattering
        length).
    """
    bc_table = copy.deepcopy(list_atom_types_elements)

    e_radius = periodictable.constants.electron_radius

    for residue in list_atom_types_elements:
        for atom_type in list_atom_types_elements[residue]:
            atom_isotope = str(list_atom_types_elements[residue][atom_type])
            select_periodictable = getattr(periodictable, atom_isotope)
            f1, f2 = select_periodictable.xray.scattering_factors(energy=xray_energy)
            atom_b = e_radius * f1 * 10 ** 15  # in fm
            atom_bi = -e_radius * f2 * 10 ** 15  # in fm

            bc_table[residue][atom_type] = (atom_isotope, atom_b, atom_bi)
    return bc_table


def list_molecules(traj):
    """List the number of molecules/single atoms in the trajectory.

    Parameters:
        traj (trajectory): The loaded simulation trajectory.

    Returns:
        molecules (list of tuples): List of all the different
        residues/molecules/ions on the simulation, and their number.

    Example:
    ::

        list_molecules(traj)

        >>> [('BR', 1114), ('bmim', 1114), ('CU1', 400), ('CU', 400)]

    """
    list_residues = [residue for residue in traj.topology.residues]
    list_residue_names = [residue.name for residue in list_residues]
    list_residue_names_unique = set([residue.name for residue in list_residues])

    molecules = [
        (residue, list_residue_names.count(residue))
        for residue in set(list_residue_names_unique)
    ]

    return molecules
