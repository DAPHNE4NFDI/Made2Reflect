FROM python:3.7-slim-buster

COPY . /app

WORKDIR /app

RUN apt-get -y update && \
    apt-get -y install build-essential libssl-dev libffi-dev libz-dev && \
    pip3 install --no-cache-dir -r ./docker-files/requirements_docker.txt && \
    apt-get -y remove --purge build-essential libssl-dev libffi-dev libz-dev && \
    apt-get -y install pymol && \
    apt-get -y install nano && \
    apt-get -y autoremove && \
    apt-get -y clean && \
    export PYTHONPATH="/usr/lib/python3/dist-packages/" && \
    echo $PYTHONPATH && \
    groupadd user && \
    useradd --create-home --shell /bin/bash -g user user && \
    echo "umask 000" >> /home/user/.profile && \
    chown -R user:user /app && \
    chmod -R g+rwx /app && chmod -R o+rwx /app


ENV PYTHONPATH="/usr/lib/python3/dist-packages/"

ENTRYPOINT ["/app/docker-files/start-docker.sh"]



