import os
import pytest
import numpy as np
import csv
from shutil import rmtree

from md2reflect import (
    load_trajectory,
    create_folder,
    display_residues,
    save_file,
    save_number_density,
    save_number_density_atom_types,
    save_SLD_elements,
    save_SLD_atom_types,
    save_SLD_total,
    save_SLD_left,
    save_SLD_right,
    save_SLD_atom_types_combination,
    show_SLD_range,
    save_reflectivity_left,
    save_reflectivity_right,
)


@pytest.fixture
def SLD_total_no_substrate():
    SLD_total_no_substrate = (
        [2.0e-7, 1.0e-7, 5.0e-8, 3.0e-8, 5.0e-8, 3.0e-8, 1.0e-8],
        [-1.0e-10, -1.0e-10, -2.0e-14, -4.0e-14, -2.0e-14, -4.0e-14, -1.0e-10],
    )
    return SLD_total_no_substrate


@pytest.fixture
def substrate_layers():
    substrate_layers = [
        (6.0e-08, -1.0e-10, 0, 0.0),
        (4e-08, -2.0e-12, 10, 0.0),
        (2e-08, -5.0e-14, 8, 0.0),
    ]
    return substrate_layers


@pytest.fixture
def environment_layers():
    environment_layers = [(4.0e-8, -3.0e-14, 0.0, 0.0)]
    return environment_layers


@pytest.fixture
def neutron_reference_example():
    # Reference neutron reflectivity curve obtained from:
    # https://www.ncnr.nist.gov/instruments/magik/calculators/reflectivity-calculator.html
    # by defining a single layer of 100A and a semi-infinite layer at each side
    # with certain roughness between them.
    path_tsvfile = os.path.join(
        "tests", "test_simulations", "reflectivity_2-1-0e-6_4-100-7A.tsv"
    )
    with open(path_tsvfile) as tsvfile:
        reader = csv.reader(tsvfile, delimiter="\t")
        qz_reference = []
        reflectivity_reference = []
        count = 0
        for row in reader:
            if count <= 1:
                count += 1
            else:
                qz_reference += [float(row[0])]
                reflectivity_reference += [float(row[1])]

    neutron_reference_example = [
        np.array(qz_reference),
        np.array(reflectivity_reference),
    ]

    return neutron_reference_example


def test_create_folder_1():
    # Test when folder does not exist
    folder_name = "create_folder_test"
    folder_path = os.path.join("tests", folder_name)
    folder_exists = os.path.exists(folder_path)

    # If folder doesn't exist it should be created
    if not folder_exists:
        files_before = get_list_of_files()
        folders_before = get_list_of_folders()
        create_folder(folder_path, autoname=False)
        files_after = get_list_of_files()
        folders_after = get_list_of_folders()

        assert files_before == files_after

        # Substract list of folders before and after
        folder_difference = folders_after.copy()
        for folder in folders_before:
            folder_difference.remove(folder)

        # Check that only this folder was created
        assert len(folder_difference) == 1
        assert folder_path in folder_difference[0]

        # Delete created folder
        os.rmdir(folder_difference[0])


def test_create_folder_2():
    # Test when folder exists with autoname option disabled
    folder_name = "create_folder_test"
    folder_path = os.path.join("tests", folder_name)
    folder_exists = os.path.exists(folder_path)

    # If folder exists nothing should happen
    if folder_exists:
        files_before = get_list_of_files()
        folders_before = get_list_of_folders()
        create_folder(folder_path, autoname=False)
        files_after = get_list_of_files()
        folders_after = get_list_of_folders()

        assert files_before == files_after
        assert folders_before == folders_after


def test_create_folder_3():
    # Test when folder exists with autoname option enabled
    folder_name = "create_folder_test"
    folder_path = os.path.join("tests", folder_name)
    folder_exists = os.path.exists(folder_path)

    # If folder exists an appropriate numbering is added to the folder name
    if folder_exists:
        files_before = get_list_of_files()
        folders_before = get_list_of_folders()
        create_folder(folder_path, autoname=True)
        files_after = get_list_of_files()
        folders_after = get_list_of_folders()

        assert files_before == files_after
        # Substract list of folders before and after
        folder_difference = folders_after.copy()
        for folder in folders_before:
            folder_difference.remove(folder)

        # Check that only this folder was created
        assert len(folder_difference) == 1
        assert folder_path in folder_difference[0]

        # Delete created folder
        os.rmdir(folder_difference[0])


def rename_test_folder(folder_name_prefix):
    # Check that folder doesn't exist. If it does, add a consecutive number to
    # the folder name. Keep increasing number until folder doesn't exist.
    # Returns the relative path to the created folder.
    folder_name_prefix_number = folder_name_prefix
    folder_number = 1
    while os.path.exists(folder_name_prefix_number):
        folder_name_prefix_number = folder_name_prefix + str(folder_number)
        folder_number += 1
    return folder_name_prefix_number


def get_list_of_files(path_to_search=os.getcwd()):
    # Obtains a full list of all the files within a folder and its subfolders.

    # Create a list of file and sub directories names in the given directory
    list_of_files = os.listdir(path_to_search)
    all_files = list()

    # Iterate over all the entries
    for entry in list_of_files:
        # Create full path
        full_path_to_search = os.path.join(path_to_search, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(full_path_to_search):
            all_files = all_files + get_list_of_files(
                path_to_search=full_path_to_search
            )
        else:
            all_files.append(full_path_to_search)

    return all_files


def get_list_of_folders(path_to_search=os.getcwd()):
    # Obtains a full list of all the folders within a folder and its
    # subfolders.

    # Create a list of file and sub directories names in the given directory
    list_of_files = os.listdir(path_to_search)
    all_folders = list()

    # Iterate over all the entries
    for entry in list_of_files:
        # Create full path
        full_path_to_search = os.path.join(path_to_search, entry)
        # If entry is a folder, add to list and get the list of folders inside
        if os.path.isdir(full_path_to_search):
            all_folders.append(full_path_to_search)
            all_folders = all_folders + get_list_of_folders(
                path_to_search=full_path_to_search
            )

    return all_folders


def check_file_types_exist(path_to_search, file_types, number_of_file_types=None):
    # Returns a dictionary with the number of files with the specified
    # extensions that exist within the folder and its subfolders. If the number
    # of expected files of each type is specified, then it returns a list with
    # boolean values.
    list_of_files = get_list_of_files(path_to_search=path_to_search)
    file_types_count = {file_type: 0 for file_type in file_types}
    for extension in file_types:
        for item in list_of_files:
            if item[-len(extension) :].lower() == extension.lower():
                file_types_count[extension] += 1

    if number_of_file_types is None:
        return file_types_count
    else:
        files_exist = [False] * len(file_types)
        file_types_inquiry = {
            file_types[index]: number_of_file_types[index]
            for index in range(len(file_types))
        }
        for index in range(len(file_types)):
            if file_types_count[file_types[index]] == (
                file_types_inquiry[file_types[index]]
            ):
                files_exist[index] = True
        return files_exist


def test_display_residues():
    folder_name_prefix = "tests_temp"
    folder_name_sufix = "residues"
    folder_name_prefix_number = rename_test_folder(folder_name_prefix)

    traj = load_trajectory(
        os.path.join("tests", "test_simulations", "traj3_5mols_4frames.pdb")
    )
    display_residues(
        traj, folder_name=os.path.join(folder_name_prefix_number, folder_name_sufix)
    )

    # Assert that the folder was created and that there are as many png and pdb
    # files as molecules in the trajectory
    assert os.path.exists(os.path.join(folder_name_prefix_number, folder_name_sufix))
    assert all(
        check_file_types_exist(
            folder_name_prefix_number, [".png", ".pdb"], number_of_file_types=[3, 3]
        )
    )

    # Delete created folder and files
    rmtree(folder_name_prefix_number)


def test_save_file():
    X = [9, 4, 6, 3]
    Y = [2, 5, 1, 0]
    file_name = os.path.join("tests", "save_file_test.txt")
    save_file(X, Y, file_name)
    with open(file_name) as f:
        lines = f.readlines()
    os.remove(file_name)
    assert lines[0][0] == "9"
    assert lines[-1][-2] == "0"


def test_save_number_density():
    folder_name_prefix = "tests_temp"
    folder_name_prefix_number = rename_test_folder(folder_name_prefix)

    number_density_elements_allframes = {
        "hydrogen": [0.004, 0.002, 0.008],
        "carbon": [0.006, 0.001, 0.0],
        "nitrogen": [0.0, 0.0, 0.002],
    }
    zbin_interval = 1

    save_number_density(
        number_density_elements_allframes,
        zbin_interval,
        folder_name_prefix=folder_name_prefix_number,
        show=True,
    )

    # Assert that a folder was created, check that there are
    # as many png and tsv number density data files as elements
    assert os.path.exists(folder_name_prefix_number)
    assert all(
        check_file_types_exist(
            folder_name_prefix_number, [".png", ".tsv"], number_of_file_types=[3, 3]
        )
    )

    # Delete created folder and files
    rmtree(folder_name_prefix_number)


def test_save_number_density_atom_types():
    folder_name_prefix = "tests_temp"
    folder_name_prefix_number = rename_test_folder(folder_name_prefix)

    number_density_atom_types_allframes = {
        "watr": {
            "H1": [0.00001, 0.0, 0.00002, 0.0],
            "H2": [0.00001, 0.0, 0.00002, 0.0],
            "O1": [0.00001, 0.0, 0.00002, 0.0],
        }
    }
    zbin_interval = 1

    save_number_density_atom_types(
        number_density_atom_types_allframes,
        zbin_interval,
        folder_name_prefix=folder_name_prefix_number,
        show=True,
    )

    # Assert that a folder was created and check that there are
    # as many png and tsv number density data files as atom types.
    assert os.path.exists(folder_name_prefix_number)
    assert all(
        check_file_types_exist(
            folder_name_prefix_number, [".png", ".tsv"], number_of_file_types=[3, 3]
        )
    )

    # Delete created folder and files
    rmtree(folder_name_prefix_number)


def test_save_SLD_elements():
    folder_name_prefix = "tests_temp"
    folder_name_prefix_number = rename_test_folder(folder_name_prefix)

    SLD_elements = {
        "hydrogen": ([3e-09, 3e-09, 3e-09, 0.0], [-3e-15, -3e-15, -3e-15, -0.0]),
        "carbon": ([1e-08, 1e-08, 1e-08, 0.0], [-1e-11, -1e-11, -1e-11, -0.0]),
        "nitrogen": ([4e-09, 4e-09, 4e-09, 0.0], [-6e-12, -6e-12, -6 - 12, -0.0]),
    }
    zbin_interval = 1

    save_SLD_elements(
        SLD_elements,
        zbin_interval,
        folder_name_prefix=folder_name_prefix_number,
        show=True,
    )

    # Assert that a folder was created and check that there are
    # as many png and tsv data files as elements
    assert os.path.exists(folder_name_prefix_number)
    assert all(
        check_file_types_exist(
            folder_name_prefix_number, [".png", ".tsv"], number_of_file_types=[3, 3]
        )
    )

    # Delete created folder and files
    rmtree(folder_name_prefix_number)


def test_save_SLD_atom_types():
    folder_name_prefix = "tests_temp"
    folder_name_prefix_number = rename_test_folder(folder_name_prefix)

    SLD_atom_types = {
        "watr": {
            "H1": ([3e-10, 0.0, 6e-10, 0.0], [-2e-16, -0.0, -4e-16, -0.0]),
            "H2": ([3e-10, 0.0, 6e-10, 0.0], [-2e-16, -0.0, -4e-16, -0.0]),
            "O1": ([2e-09, 0.0, 4e-09, 0.0], [-5e-12, -0.0, -1e-11, -0.0]),
        }
    }
    zbin_interval = 1

    save_SLD_atom_types(
        SLD_atom_types,
        zbin_interval,
        sufix="",
        folder_name_prefix=folder_name_prefix_number,
        show=True,
    )

    # Assert that a folder was created and check that there are
    # as many png and tsv data files as atom types
    assert os.path.exists(folder_name_prefix_number)
    assert all(
        check_file_types_exist(
            folder_name_prefix_number, [".png", ".tsv"], number_of_file_types=[3, 3]
        )
    )

    # Delete created folder and files
    rmtree(folder_name_prefix_number)


def test_save_SLD_total(SLD_total_no_substrate):
    folder_name_prefix = "tests_temp"
    folder_name_prefix_number = rename_test_folder(folder_name_prefix)

    SLD_total = (
        [2.0e-6, 1.0e-7, 5.0e-8, 3.0e-8, 5.0e-8, 3.0e-8, 2.0e-6],
        [-1.0e-9, -1.0e-10, -2.0e-14, -4.0e-14, -2.0e-14, -4.0e-14, -1.0e-9],
    )
    zbin_interval = 1

    save_SLD_total(
        SLD_total,
        SLD_total_no_substrate,
        zbin_interval,
        folder_name_prefix=folder_name_prefix_number,
        show=True,
    )

    # Assert that a folder, and a png and tsv data files for each SLD were
    # created
    assert os.path.exists(folder_name_prefix_number)
    assert all(
        check_file_types_exist(
            folder_name_prefix_number, [".png", ".tsv"], number_of_file_types=[2, 2]
        )
    )

    # Delete created folder and files
    rmtree(folder_name_prefix_number)


def test_save_SLD_left(substrate_layers, environment_layers):
    folder_name_prefix = "tests_temp"
    folder_name_prefix_number = rename_test_folder(folder_name_prefix)

    SLD_tuned_left = (
        np.array(
            [
                6.0e-08,
                4.0e-08,
                2.0e-08,
                1.0e-07,
                5.0e-08,
                3.0e-08,
                5.0e-08,
                3.0e-08,
                4.0e-08,
            ]
        ),
        np.array(
            [
                -1.0e-10,
                -2.0e-12,
                -5.0e-14,
                -1.0e-10,
                -2.0e-14,
                -4.0e-14,
                -2.0e-14,
                -4.0e-14,
                -3.0e-14,
            ]
        ),
    )
    zbin_interval = 1

    save_SLD_left(
        SLD_tuned_left,
        substrate_layers,
        environment_layers,
        zbin_interval,
        semi_infinite_zbins=20,
        folder_name_prefix=folder_name_prefix_number,
        show=True,
    )

    # Assert that a folder, and a jpg and a tsv data files were created
    assert os.path.exists(folder_name_prefix_number)
    assert all(
        check_file_types_exist(
            folder_name_prefix_number, [".png", ".tsv"], number_of_file_types=[1, 1]
        )
    )

    # Delete created folder and files
    rmtree(folder_name_prefix_number)


def test_save_SLD_right(substrate_layers, environment_layers):
    folder_name_prefix = "tests_temp"
    folder_name_prefix_number = rename_test_folder(folder_name_prefix)

    SLD_tuned_right = (
        np.array(
            [
                4.0e-08,
                5.0e-08,
                3.0e-08,
                5.0e-08,
                3.0e-08,
                1.0e-08,
                2.0e-08,
                4.0e-08,
                6.0e-08,
            ]
        ),
        np.array(
            [
                -3.0e-14,
                -2.0e-14,
                -4.0e-14,
                -2.0e-14,
                -4.0e-14,
                -1.0e-10,
                -5.0e-14,
                -2.0e-12,
                -1.0e-10,
            ]
        ),
    )
    zbin_interval = 1

    save_SLD_right(
        SLD_tuned_right,
        substrate_layers,
        environment_layers,
        zbin_interval,
        semi_infinite_zbins=20,
        folder_name_prefix=folder_name_prefix_number,
        show=True,
    )

    # Assert that a folder, and a jpg and a tsv data files were created
    assert os.path.exists(folder_name_prefix_number)
    assert all(
        check_file_types_exist(
            folder_name_prefix_number, [".png", ".tsv"], number_of_file_types=[1, 1]
        )
    )

    # Delete created folder and files
    rmtree(folder_name_prefix_number)


def test_save_SLD_atom_types_combination():
    folder_name_prefix = "tests_temp"
    folder_name_prefix_number = rename_test_folder(folder_name_prefix)

    SLD_atom_types_combination = (
        [6e-10, 3e-10, 1.2e-09, 6e-10],
        [-4e-16, -2e-16, -8e-16, -4e-16],
    )
    zbin_interval = 1

    save_SLD_atom_types_combination(
        SLD_atom_types_combination,
        zbin_interval,
        folder_name_prefix=folder_name_prefix_number,
        show=True,
    )

    # Assert that a folder, and a jpg and a tsv data files were created
    assert os.path.exists(folder_name_prefix_number)
    assert all(
        check_file_types_exist(
            folder_name_prefix_number, [".png", ".tsv"], number_of_file_types=[1, 1]
        )
    )

    # Delete created folder and files
    rmtree(folder_name_prefix_number)


def test_show_SLD_range(SLD_total_no_substrate):
    folder_name_prefix = "tests_temp"
    folder_name_prefix_number = rename_test_folder(folder_name_prefix)

    SLD_profile = SLD_total_no_substrate[0]
    zbin_interval = 1

    show_SLD_range(
        SLD_profile,
        zbin_interval,
        starting_point=2,
        ending_point=5,
        distance_input=True,
        folder_name_prefix=folder_name_prefix_number,
        show=True,
    )

    # Assert that a folder, and a jpg files were created
    assert os.path.exists(folder_name_prefix_number)
    assert all(
        check_file_types_exist(
            folder_name_prefix_number, [".png"], number_of_file_types=[1]
        )
    )

    # Delete created folder and files
    rmtree(folder_name_prefix_number)


def test_save_reflectivity_left(neutron_reference_example):
    folder_name_prefix = "tests_temp"
    folder_name_prefix_number = rename_test_folder(folder_name_prefix)

    reflectivity_MD_left = neutron_reference_example[1]

    save_reflectivity_left(
        reflectivity_MD_left,
        q_max=0.25,
        q_number_points=250,
        folder_name_prefix=folder_name_prefix_number,
        show=True,
    )

    # Assert that a folder, and a jpg and a tsv data files were created
    assert os.path.exists(folder_name_prefix_number)
    assert all(
        check_file_types_exist(
            folder_name_prefix_number, [".png", ".tsv"], number_of_file_types=[1, 1]
        )
    )

    # Delete created folder and files
    rmtree(folder_name_prefix_number)


def test_save_reflectivity_right(neutron_reference_example):
    folder_name_prefix = "tests_temp"
    folder_name_prefix_number = rename_test_folder(folder_name_prefix)

    reflectivity_MD_right = neutron_reference_example[1]

    save_reflectivity_right(
        reflectivity_MD_right,
        q_max=0.25,
        q_number_points=250,
        folder_name_prefix=folder_name_prefix_number,
        show=True,
    )

    # Assert that a folder, and a jpg and a tsv data files were created
    assert os.path.exists(folder_name_prefix_number)
    assert all(
        check_file_types_exist(
            folder_name_prefix_number, [".png", ".tsv"], number_of_file_types=[1, 1]
        )
    )

    # Delete created folder and files
    rmtree(folder_name_prefix_number)
