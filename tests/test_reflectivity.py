import os
import pytest
import numpy as np
import csv


from md2reflect import (
    invert_SLD,
    refl,
    calculate_reflectivity,
    reflectivity_selector,
)


def test_invert_SLD():
    SLD_tuned_left = (
        np.array(
            [
                6.0e-08,
                4.0e-08,
                2.0e-08,
                1.0e-07,
                5.0e-08,
                3.0e-08,
                5.0e-08,
                3.0e-08,
                4.0e-08,
            ]
        ),
        np.array(
            [
                -1.0e-10,
                -2.0e-12,
                -5.0e-14,
                -1.0e-10,
                -2.0e-14,
                -4.0e-14,
                -2.0e-14,
                -4.0e-14,
                -3.0e-14,
            ]
        ),
    )

    SLD_tuned_left_inverted_true_values = (
        np.array(
            [
                4.0e-08,
                3.0e-08,
                5.0e-08,
                3.0e-08,
                5.0e-08,
                1.0e-07,
                2.0e-08,
                4.0e-08,
                6.0e-08,
            ]
        ),
        np.array(
            [
                -3.0e-14,
                -4.0e-14,
                -2.0e-14,
                -4.0e-14,
                -2.0e-14,
                -1.0e-10,
                -5.0e-14,
                -2.0e-12,
                -1.0e-10,
            ]
        ),
    )

    SLD_tuned_left_inverted = invert_SLD(SLD_tuned_left)

    assert len(SLD_tuned_left_inverted) == len(SLD_tuned_left_inverted_true_values)
    assert len(SLD_tuned_left_inverted[0]) == len(SLD_tuned_left[0])
    assert len(SLD_tuned_left_inverted[1]) == len(SLD_tuned_left[1])

    for part in range(2):
        for item in range(len(SLD_tuned_left[part])):
            assert SLD_tuned_left_inverted[part][item] == (
                SLD_tuned_left[part][len(SLD_tuned_left[part]) - 1 - item]
            )

    assert np.array_equal(invert_SLD(invert_SLD(SLD_tuned_left)), SLD_tuned_left)


@pytest.fixture
def neutron_reference_example():
    # Reference neutron reflectivity curve obtained from:
    # https://www.ncnr.nist.gov/instruments/magik/calculators/reflectivity-calculator.html
    # by defining a single layer of 100A and a semi-infinite layer at each side
    # with certain roughness between them.
    path_tsvfile = os.path.join(
        "tests", "test_simulations", "reflectivity_2-1-0e-6_4-100-7A.tsv"
    )
    with open(path_tsvfile) as tsvfile:
        reader = csv.reader(tsvfile, delimiter="\t")
        qz_reference = []
        reflectivity_reference = []
        count = 0
        for row in reader:
            if count <= 1:
                count += 1
            else:
                qz_reference += [float(row[0])]
                reflectivity_reference += [float(row[1])]

    neutron_reference_example = [
        np.array(qz_reference),
        np.array(reflectivity_reference),
    ]

    return neutron_reference_example


def test_refl(neutron_reference_example):
    # Comparison with the reference neutron reflectivity curve obtained from:
    # https://www.ncnr.nist.gov/instruments/magik/calculators/reflectivity-calculator.html
    # by defining a single layer of 100A and a semi-infinite layer at each side
    # with certain roughness between them.

    # We compute the reflectivity for the same qz values as the reference
    qz = neutron_reference_example[0]
    sld = np.array([0, 1e-6, 2e-6])
    sld_abs = np.array([0, 0, 0])
    thickness = np.array([0, 100, np.inf])
    roughness = np.array([7, 4])

    reflectivity_calculation = abs(refl(qz, sld, sld_abs, thickness, roughness)) ** 2

    assert reflectivity_calculation == pytest.approx(
        np.array(neutron_reference_example[1]), rel=1e-10
    )


def test_calculate_reflectivity(neutron_reference_example):
    # Comparison with the reference neutron reflectivity curve obtained from:
    # https://www.ncnr.nist.gov/instruments/magik/calculators/reflectivity-calculator.html
    # with a single layer of 100A and a semi-infinite layer at each side with
    # certain roughness between them but defined by slicing the sld in many
    # equally spaced layers (1 Angstrom) without intermediate roughness.
    path_tsvfile = os.path.join(
        "tests", "test_simulations", "sld_2-1-0e-6_4-100-7A.tsv"
    )
    with open(path_tsvfile) as tsvfile:
        reader = csv.reader(tsvfile, delimiter="\t")
        z_reference = []
        sld_reference = []
        count = 0
        for row in reader:
            if count <= 0:
                count += 1
            else:
                z_reference += [float(row[0])]
                sld_reference += [float(row[1]) * 1e-6]

    SLD_tuned = (np.array(sld_reference), np.zeros(133))
    zbin_interval = 1

    reflectivity_calculation = calculate_reflectivity(
        SLD_tuned,
        zbin_interval,
        incoming_beam="right",
        left_layers=None,
        right_layers=None,
        q_max=0.25,
        q_number_points=250,
    )

    # Allow almost no discrepancy before the edge
    assert reflectivity_calculation[1:9] == pytest.approx(
        np.array(neutron_reference_example[1][0:8]), rel=1e-6
    )

    # Allow more difference around and just after the edge
    assert reflectivity_calculation[9:32] == pytest.approx(
        np.array(neutron_reference_example[1][8:31]), rel=0.30
    )

    # Allow smaller discrepancy after the first minimum
    assert reflectivity_calculation[32:250] == pytest.approx(
        np.array(neutron_reference_example[1][31:249]), rel=0.20
    )


@pytest.fixture
def reflectivity_constant_resolution_example():
    # Simulated reflectivity as measured at a TOF reflectometer using optically
    # blind choppers (constant dq_q resolution) and a purposefully high
    # background, with the following parameters:

    # Points where we want to evaluate R (Angstrom^-1):
    # q = np.linspace(0,.1,1000)

    # Atmosphere/vacuum: SLD = 0
    # Atmosphere/vacuum-layer roughness sigma = 2
    # Layer: SLD = 2e-6, thickness = 200 Angstrom
    # Layer-substrate roughness sigma = 1
    # Substrate: SLD = 3e-6
    # (We assume the SLD absorption / imaginary part to be zero.)

    # Experimental parameters:
    # dq_q=.06, bckg= 5e-6
    path_csvfile = os.path.join(
        "tests", "test_simulations", "R_cst_dq_q_3-2-0e-6_1-200-2A.csv"
    )
    with open(path_csvfile) as csvfile:
        reader = csv.reader(csvfile, delimiter=",")
        qz_reference = []
        reflectivity_reference = []
        count = 0
        for row in reader:
            if count <= 1:
                count += 1
            else:
                qz_reference += [float(row[0])]
                reflectivity_reference += [float(row[1])]

    reflectivity_constant_resolution_example = [
        np.array(qz_reference),
        np.array(reflectivity_reference),
    ]

    return reflectivity_constant_resolution_example


@pytest.fixture
def reflectivity_monochromatic_example():
    # Simulated reflectivity as measured at a monochromatic instrument and a
    # purposefully high background, with the following parameters:

    # Points where we want to evaluate R (Angstrom^-1):
    # q = np.linspace(0,.1,1000)

    # Atmosphere/vacuum: SLD = 0
    # Atmosphere/vacuum-layer roughness sigma = 2
    # Layer: SLD = 2e-6, thickness = 200 Angstrom
    # Layer-substrate roughness sigma = 1
    # Substrate: SLD = 3e-6
    # (We assume the SLD absorption / imaginary part to be zero.)

    # Experimental parameters:
    # wl = 1, dwl=.03, dtheta=.005, bckg= 5e-6

    path_csvfile = os.path.join(
        "tests", "test_simulations", "R_monochr_3-2-0e-6_1-200-2A.csv"
    )
    with open(path_csvfile) as csvfile:
        reader = csv.reader(csvfile, delimiter=",")
        qz_reference = []
        reflectivity_reference = []
        count = 0
        for row in reader:
            if count <= 1:
                count += 1
            else:
                if row[1] != "":
                    qz_reference += [float(row[0])]
                    reflectivity_reference += [float(row[1])]

    reflectivity_monochromatic_example = [
        np.array(qz_reference),
        np.array(reflectivity_reference),
    ]

    return reflectivity_monochromatic_example


def test_reflectivity_selector_theoretical(neutron_reference_example):
    # Comparison with the reference neutron reflectivity curve obtained from:
    # https://www.ncnr.nist.gov/instruments/magik/calculators/reflectivity-calculator.html
    # by defining a single layer of 100A and a semi-infinite layer at each side
    # with certain roughness between them.

    # We compute the reflectivity for the same qz values as the reference
    qz = neutron_reference_example[0]
    sld = np.array([0, 1e-6, 2e-6])
    sld_abs = np.array([0, 0, 0])
    thickness = np.array([0, 100, np.inf])
    roughness = np.array([7, 4])

    reflectivity_calculation = reflectivity_selector(
        qz, sld, sld_abs, thickness, roughness, reflectivity_function="theoretical"
    )

    assert reflectivity_calculation == pytest.approx(
        np.array(neutron_reference_example[1]), rel=1e-10
    )


def test_reflectivity_selector_abeles_theoretical(neutron_reference_example):
    # Comparison with the reference neutron reflectivity curve obtained from:
    # https://www.ncnr.nist.gov/instruments/magik/calculators/reflectivity-calculator.html
    # by defining a single layer of 100A and a semi-infinite layer at each side
    # with certain roughness between them.

    # We compute the reflectivity for the same qz values as the reference
    qz = neutron_reference_example[0]
    sld = np.array([0, 1e-6, 2e-6])
    sld_abs = np.array([0, 0, 0])
    thickness = np.array([0, 100, np.inf])
    roughness = np.array([7, 4])

    reflectivity_calculation = reflectivity_selector(
        qz,
        sld,
        sld_abs,
        thickness,
        roughness,
        reflectivity_function="abeles_theoretical",
    )

    assert reflectivity_calculation == pytest.approx(
        np.array(neutron_reference_example[1]), rel=1e-10
    )


def test_reflectivity_selector_abeles_constant_resolution(
    reflectivity_constant_resolution_example,
):
    # Simulated reflectivity as measured at a TOF reflectometer using optically
    # blind choppers (constant dq_q resolution) and a purposefully high
    # background, with the following parameters:

    # Points where we want to evaluate R (Angstrom^-1):
    # q = np.linspace(0,.1,1000)

    # Atmosphere/vacuum: SLD = 0
    # Atmosphere/vacuum-layer roughness sigma = 2
    # Layer: SLD = 2e-6, thickness = 200 Angstrom
    # Layer-substrate roughness sigma = 1
    # Substrate: SLD = 3e-6
    # (We assume the SLD absorption / imaginary part to be zero.)

    # Experimental parameters:
    # dq_q=.06, bckg= 5e-6

    # We compute the reflectivity for the same qz values as the example
    qz = reflectivity_constant_resolution_example[0]
    sld = np.array([0, 2e-6, 3e-6])
    sld_abs = np.array([0, 0, 0])
    thickness = np.array([0, 200, np.inf])
    roughness = np.array([2, 1.0])
    background = 5e-6
    resolution = 0.06

    instrument_parameters = {"dq_q": resolution, "bckg": background}

    reflectivity_calculation = reflectivity_selector(
        qz,
        sld,
        sld_abs,
        thickness,
        roughness,
        reflectivity_function="abeles_constant_resolution",
        **instrument_parameters
    )

    assert reflectivity_calculation == pytest.approx(
        np.array(reflectivity_constant_resolution_example[1]), rel=1e-10
    )


def test_reflectivity_selector_abeles_monochromatic(reflectivity_monochromatic_example):
    # Simulated reflectivity as measured at a monochromatic instrument and a
    # purposefully high background, with the following parameters:

    # Points where we want to evaluate R (Angstrom^-1):
    # q = np.linspace(0,.1,1000)

    # Atmosphere/vacuum: SLD = 0
    # Atmosphere/vacuum-layer roughness sigma = 2
    # Layer: SLD = 2e-6, thickness = 200 Angstrom
    # Layer-substrate roughness sigma = 1
    # Substrate: SLD = 3e-6
    # (We assume the SLD absorption / imaginary part to be zero.)

    # Experimental parameters:
    # wl = 1, dwl=.03, dtheta=.005, bckg= 5e-6

    # We compute the reflectivity for the same qz values as the example
    qz = reflectivity_monochromatic_example[0]
    sld = np.array([0, 2e-6, 3e-6])
    sld_abs = np.array([0, 0, 0])
    thickness = np.array([0, 200, np.inf])
    roughness = np.array([2, 1.0])
    background = 5e-6
    wavelength = 1
    dwavelength = 0.03
    dtheta = 0.005
    background = 5e-6

    instrument_parameters = {
        "bckg": background,
        "wl": wavelength,
        "dwl": dwavelength,
        "dtheta": dtheta,
    }

    reflectivity_calculation = reflectivity_selector(
        qz,
        sld,
        sld_abs,
        thickness,
        roughness,
        reflectivity_function="abeles_monochromatic",
        **instrument_parameters
    )

    assert reflectivity_calculation == pytest.approx(
        np.array(reflectivity_monochromatic_example[1]), rel=1e-10
    )
