from md2reflect import (
    version,
    check_compatibility,
)


def test_version():
    version_output = version()
    assert len(version_output) > 0
    assert type(version_output) == str


def test_check_compatibility(capsys):
    check_compatibility()
    captured = capsys.readouterr().out
    assert type(captured) == str
    assert ("Python" and "Numpy" and "Matplotlib" and "MDTraj") in captured
