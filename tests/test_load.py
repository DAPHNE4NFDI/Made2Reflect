import os
import pytest


from md2reflect import (
    load_trajectory,
    chop_trajectory,
    get_trajectory_chunk,
)


# This decorator lists de combination of parameters to be tested in the
# decorated function.
@pytest.mark.parametrize(
    "files, filename_topology_param, skip_frames_param, "
    "number_frames, xcoord_first, zcoord_last",
    [
        # Test loading a single pdb file
        (
            os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb"),
            "",
            1,
            4,
            15.320,
            85.880,
        ),
        # Test loading a single pdb file and skipping frames
        (
            os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb"),
            "",
            2,
            2,
            15.320,
            88.760,
        ),
        # Test loading multiple pdb files
        (
            [
                os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb"),
                os.path.join("tests", "test_simulations", "traj3_3mols_1frame.pdb"),
            ],
            "",
            1,
            5,
            15.320,
            89.150,
        ),
        # Test loading a single dcd file
        (
            os.path.join("tests", "test_simulations", "traj3_3mols_4frames.dcd"),
            os.path.join("tests", "test_simulations", "traj3_3mols_1frame.pdb"),
            1,
            4,
            15.320,
            85.880,
        ),
        # Test loading a single dcd file and skipping frames
        (
            os.path.join("tests", "test_simulations", "traj3_3mols_4frames.dcd"),
            os.path.join("tests", "test_simulations", "traj3_3mols_1frame.pdb"),
            2,
            2,
            15.320,
            88.760,
        ),
        # Test loading multiple dcd files
        (
            [
                os.path.join("tests", "test_simulations", "traj3_3mols_4frames.dcd"),
                os.path.join("tests", "test_simulations", "traj3_3mols_1frame.dcd"),
            ],
            os.path.join("tests", "test_simulations", "traj3_3mols_1frame.pdb"),
            1,
            5,
            15.320,
            89.150,
        ),
    ],
)
def test_load_trajectory(
    files,
    filename_topology_param,
    skip_frames_param,
    number_frames,
    xcoord_first,
    zcoord_last,
):
    # Checks if the loaded trajectories have the right number of frames, the x
    # coordinate of the first atom of the first frame of the trajectory, and
    # the z coordinate of the last atom of the last frame of the trajectory.

    traj = load_trajectory(
        files, filename_topology=filename_topology_param, skip_frames=skip_frames_param
    )

    first_atom_x_true_value = xcoord_first
    last_atom_z_true_value = zcoord_last

    first_atom_x = traj.xyz[0, 0, 0] * 10
    last_atom_z = traj.xyz[len(traj) - 1, traj.n_atoms - 1, 2] * 10

    assert len(traj) == number_frames
    assert first_atom_x == pytest.approx(first_atom_x_true_value)
    assert last_atom_z == pytest.approx(last_atom_z_true_value)


# This decorator lists de combination of parameters to be tested in the
# decorated function.
@pytest.mark.parametrize(
    "files, filename_topology_param, skip_frames_param, "
    "frame_chunk_param, total_chunks, "
    "total_number_frames, xcoord_first, zcoord_last",
    [
        # Test loading a single pdb file, no skipping frames, chunks of
        # 2 frames
        (
            os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb"),
            "",
            1,  # skip_frames_param,
            2,  # frame_chunk_param
            2,  # total_chunks
            4,  # total_number_frames
            15.320,
            85.880,
        ),
        # Test loading a single pdb file, skipping frames, chunks of 1 frame
        (
            os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb"),
            "",
            2,
            1,
            2,
            2,
            15.320,
            88.760,
        ),
        # Test loading a single pdb file, skipping frames, chunks of 2 frames
        (
            os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb"),
            "",
            2,
            2,
            1,
            2,
            15.320,
            88.760,
        ),
        # Test loading multiple pdb files, skipping frames, chunks of 2 frames
        # (frames from different files are not mixed in a single chunk)
        (
            [
                os.path.join("tests", "test_simulations", "traj3_3mols_1frame.pdb"),
                os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb"),
            ],
            "",
            1,
            2,
            3,
            5,
            15.320,
            85.880,
        ),
        # Test loading a single dcd file, no skipping frames, chunks of
        # 2 frames
        (
            os.path.join("tests", "test_simulations", "traj3_3mols_4frames.dcd"),
            os.path.join("tests", "test_simulations", "traj3_3mols_1frame.pdb"),
            1,  # skip_frames_param,
            2,  # frame_chunk_param
            2,  # total_chunks
            4,  # total_number_frames
            15.320,
            85.880,
        ),
        # Test loading a single dcd file, skipping frames, chunks of 1 frame
        (
            os.path.join("tests", "test_simulations", "traj3_3mols_4frames.dcd"),
            os.path.join("tests", "test_simulations", "traj3_3mols_1frame.pdb"),
            2,
            1,
            2,
            2,
            15.320,
            88.760,
        ),
        # Test loading a single dcd file, skipping frames, chunks of 2 frames
        (
            os.path.join("tests", "test_simulations", "traj3_3mols_4frames.dcd"),
            os.path.join("tests", "test_simulations", "traj3_3mols_1frame.pdb"),
            2,
            2,
            1,
            2,
            15.320,
            88.760,
        ),
        # Test loading multiple pdb files, skipping frames, chunks of 2 frames
        # (frames from different files are not mixed in a single chunk)
        (
            [
                os.path.join("tests", "test_simulations", "traj3_3mols_1frame.dcd"),
                os.path.join("tests", "test_simulations", "traj3_3mols_4frames.dcd"),
            ],
            os.path.join("tests", "test_simulations", "traj3_3mols_1frame.pdb"),
            1,
            2,
            3,
            5,
            15.320,
            85.880,
        ),
    ],
)
def test_chop_trajectory(
    files,
    filename_topology_param,
    skip_frames_param,
    frame_chunk_param,
    total_chunks,
    total_number_frames,
    xcoord_first,
    zcoord_last,
):
    # Checks if the loaded trajectory chunks have the right number of frames,
    # the x coordinate of the first atom of the first frame of the first chunk,
    # and the z coordinate of the last atom of the last frame of the last
    # chunk.

    trajectory_chunks_generator = chop_trajectory(
        files,
        filename_topology=filename_topology_param,
        skip_frames=skip_frames_param,
        frame_chunk=frame_chunk_param,
        chunk_fix=True,
    )

    first_atom_x_true_value = xcoord_first
    last_atom_z_true_value = zcoord_last

    counter_total_frames = 0
    counter_chunks = 0
    for chunk in trajectory_chunks_generator:
        counter_chunk_frames = 0
        for frame in chunk:
            if counter_total_frames == 0 and counter_chunk_frames == 0:
                first_atom_x = frame.xyz[0, 0, 0] * 10
            last_atom_z = frame.xyz[0, frame.n_atoms - 1, 2] * 10
            counter_chunk_frames += 1
        counter_total_frames += counter_chunk_frames
        counter_chunks += 1

    assert len(chunk) == frame_chunk_param
    assert counter_total_frames == total_number_frames
    assert first_atom_x == pytest.approx(first_atom_x_true_value)
    assert last_atom_z == pytest.approx(last_atom_z_true_value)


def test_get_trajectory_chunk():

    trajectory_chunks_generator = chop_trajectory(
        os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb"),
        skip_frames=2,
        frame_chunk=2,
    )
    chunk = get_trajectory_chunk(trajectory_chunks_generator)

    first_atom_x_true_value = 15.320
    last_atom_z_true_value = 88.760

    first_atom_x = chunk.xyz[0, 0, 0] * 10
    last_atom_z = chunk.xyz[len(chunk) - 1, chunk.n_atoms - 1, 2] * 10

    assert len(chunk) == 2
    assert first_atom_x == pytest.approx(first_atom_x_true_value)
    assert last_atom_z == pytest.approx(last_atom_z_true_value)
