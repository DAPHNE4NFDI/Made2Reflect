import os
import pytest


from md2reflect import (
    load_trajectory,
    chop_trajectory,
    select_zbinning,
    count_number_atoms,
    count_number_atoms_chunks,
    count_number_atoms_types,
    count_number_atoms_types_chunks,
    calculate_number_density_elements,
    calculate_number_density_atom_types,
)


def test_select_zbinning():
    traj = load_trajectory(
        os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb")
    )

    box_zlength_true_value = 160

    # Test binning by interval
    zbin_interval_true_value = 0.5
    zbins_number_true_value = 320
    zbin_volume_true_value = 1242.5

    zbin_interval, box_zlength, zbins_number, zbin_volume = select_zbinning(
        traj, zbin_interval=zbin_interval_true_value
    )

    assert zbin_interval == zbin_interval_true_value
    assert box_zlength == box_zlength_true_value
    assert zbins_number == zbins_number_true_value
    assert zbin_volume == pytest.approx(zbin_volume_true_value, rel=1e-5)

    # Test binning by number of bins
    zbin_interval_true_value = 1.0
    zbins_number_true_value = 160
    zbin_volume_true_value = 2485.0

    zbin_interval, box_zlength, zbins_number, zbin_volume = select_zbinning(
        traj, zbins_number=zbins_number_true_value
    )

    assert zbin_interval == zbin_interval_true_value
    assert box_zlength == box_zlength_true_value
    assert zbins_number == zbins_number_true_value
    assert zbin_volume == pytest.approx(zbin_volume_true_value, rel=1e-5)


def test_count_number_atoms():
    traj = load_trajectory(
        os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb")
    )
    number_atoms_elements_allframes = count_number_atoms(traj, 4)

    all_elements = list(number_atoms_elements_allframes.keys())
    for element_key in all_elements:
        assert element_key in "carbon, hydrogen, nitrogen"

    for element in number_atoms_elements_allframes:
        assert len(number_atoms_elements_allframes[element]) == 4

    assert number_atoms_elements_allframes == {
        "hydrogen": [15.0, 15.25, 14.75, 0],
        "carbon": [8.0, 8.0, 8.0, 0],
        "nitrogen": [2.0, 2.0, 2.0, 0],
    }


def test_count_number_atoms_chunks():
    trajectory_chunks_generator = chop_trajectory(
        os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb"),
        skip_frames=2,
        frame_chunk=2,
    )
    number_atoms_elements_allframes = count_number_atoms_chunks(
        trajectory_chunks_generator, 4
    )

    all_elements = list(number_atoms_elements_allframes.keys())
    for element_key in all_elements:
        assert element_key in "carbon, hydrogen, nitrogen"

    for element in number_atoms_elements_allframes:
        assert len(number_atoms_elements_allframes[element]) == 4

    assert number_atoms_elements_allframes == {
        "hydrogen": [15.0, 15.0, 15.0, 0.0],
        "carbon": [8.0, 8.0, 8.0, 0.0],
        "nitrogen": [2.0, 2.0, 2.0, 0.0],
    }


def test_count_number_atoms_types():
    traj = load_trajectory(
        os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb")
    )
    number_atoms_types_allframes = count_number_atoms_types(traj, 4)

    all_atom_types = list(number_atoms_types_allframes["bmim"].keys())

    for atom_type_key in all_atom_types:
        assert atom_type_key in (
            "C1 C10 C2 C3 C4 C5 C6 C7 N8 N9 "
            "H H11 H12 H13 H14 H15 H16 H17 H18 H19 H20 H21 H22 H23 H24"
        )

    for atom_type in number_atoms_types_allframes["bmim"]:
        assert len(number_atoms_types_allframes["bmim"][atom_type]) == 4
        if atom_type == "H22":
            assert number_atoms_types_allframes["bmim"]["H22"] == [1.0, 1.25, 0.75, 0]
        else:
            assert number_atoms_types_allframes["bmim"][atom_type] == [1.0, 1.0, 1.0, 0]


def test_count_number_atoms_types_chunks():
    trajectory_chunks_generator = chop_trajectory(
        os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb"),
        skip_frames=2,
        frame_chunk=2,
    )
    number_atoms_types_allframes = count_number_atoms_types_chunks(
        trajectory_chunks_generator, 4
    )

    all_atom_types = list(number_atoms_types_allframes["bmim"].keys())

    for atom_type_key in all_atom_types:
        assert atom_type_key in (
            "C1 C10 C2 C3 C4 C5 C6 C7 N8 N9 "
            "H H11 H12 H13 H14 H15 H16 H17 H18 H19 H20 H21 H22 H23 H24"
        )

    for atom_type in number_atoms_types_allframes["bmim"]:
        assert len(number_atoms_types_allframes["bmim"][atom_type]) == 4
        assert number_atoms_types_allframes["bmim"][atom_type] == [1.0, 1.0, 1.0, 0]


def test_calculate_number_density_elements():
    number_atoms_elements_allframes = {
        "hydrogen": [15.0, 15.0, 15.0, 0.0],
        "carbon": [8.0, 8.0, 8.0, 0.0],
        "nitrogen": [2.0, 2.0, 2.0, 0.0],
    }
    number_density_elements_allframes = calculate_number_density_elements(
        number_atoms_elements_allframes, 100000
    )

    assert number_density_elements_allframes == {
        "hydrogen": [0.00015, 0.00015, 0.00015, 0.0],
        "carbon": [0.00008, 0.00008, 0.00008, 0.0],
        "nitrogen": [0.00002, 0.00002, 0.00002, 0.0],
    }


def test_calculate_number_density_atom_types():
    number_atoms_types_allframes = {
        "molecule": {
            "H1": [1.0, 0.0, 2.0, 0.0],
            "H2": [1.0, 0.0, 2.0, 0.0],
            "O": [1.0, 0.0, 2.0, 0.0],
        }
    }
    number_density_atom_types_allframes = calculate_number_density_atom_types(
        number_atoms_types_allframes, 100000
    )

    assert number_density_atom_types_allframes == {
        "molecule": {
            "H1": [0.00001, 0.0, 0.00002, 0.0],
            "H2": [0.00001, 0.0, 0.00002, 0.0],
            "O": [0.00001, 0.0, 0.00002, 0.0],
        }
    }
