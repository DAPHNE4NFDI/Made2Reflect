import pytest
import numpy as np


from md2reflect import (
    calculate_substrate_SLD,
    build_substrate_layer,
    average_environment_SLD,
    build_average_environmental_layer,
    build_SLD_left,
    build_SLD_right,
)


def test_calculate_substrate_SLD():
    substrate_element = "silicon"
    SLD_substrate_n = calculate_substrate_SLD(substrate_element, probe="neutrons")
    SLD_substrate_x = calculate_substrate_SLD(
        substrate_element, probe="xrays", xray_energy=10
    )

    assert SLD_substrate_n[0] == pytest.approx(2.07e-6, rel=1e-2)
    assert SLD_substrate_n[1] == pytest.approx(0.0e-6, rel=1e-2)

    assert SLD_substrate_x[0] == pytest.approx(1.998e-5, rel=1e-3)
    assert SLD_substrate_x[1] == pytest.approx(-3.007e-7, rel=1e-3)


def test_build_substrate_layer():
    substrate_element = "silicon"
    substrate_layer_n = build_substrate_layer(
        substrate_element, probe="neutrons", xray_energy=8
    )
    substrate_layer_x = build_substrate_layer(
        substrate_element, probe="xrays", xray_energy=10
    )

    assert type(substrate_layer_n) is list
    assert substrate_layer_n[0][0] == pytest.approx(2.07e-6, rel=1e-2)
    assert substrate_layer_n[0][1] == pytest.approx(0.0e-6, rel=1e-2)
    assert substrate_layer_n[0][2] == 0.0

    assert type(substrate_layer_x) is list
    assert substrate_layer_x[0][0] == pytest.approx(1.998e-5, rel=1e-3)
    assert substrate_layer_x[0][1] == pytest.approx(-3.007e-7, rel=1e-3)
    assert substrate_layer_x[0][2] == 0.0


@pytest.fixture
def SLD_total_no_substrate():
    SLD_total_no_substrate = (
        [2.0e-7, 1.0e-7, 5.0e-8, 3.0e-8, 5.0e-8, 3.0e-8, 1.0e-8],
        [-1.0e-10, -1.0e-10, -2.0e-14, -4.0e-14, -2.0e-14, -4.0e-14, -1.0e-10],
    )
    return SLD_total_no_substrate


@pytest.mark.parametrize(
    "zbin_from_param, zbin_to_param, " "distance_input_param, zbin_interval_param",
    # Test interval by zbin index
    [
        (2, 6, False, None),
        # Test interval by zbin axis position
        (10, 30, True, 5),
        # Test interval by zbin axis position
        (12.5, 22.5, True, 5),
    ],
)
def test_average_environment_SLD(
    SLD_total_no_substrate,
    zbin_from_param,
    zbin_to_param,
    distance_input_param,
    zbin_interval_param,
):

    SLD_environment_index = average_environment_SLD(
        SLD_total_no_substrate,
        zbin_from_param,
        zbin_to_param,
        distance_input=distance_input_param,
        zbin_interval=zbin_interval_param,
    )

    assert len(SLD_environment_index) == 2
    assert SLD_environment_index[0] == 4.0e-8
    assert SLD_environment_index[1] == -3.0e-14


@pytest.mark.parametrize(
    "zbin_from_param, zbin_to_param, " "distance_input_param, zbin_interval_param",
    # Test interval by zbin index
    [
        (2, 6, False, None),
        # Test interval by zbin axis position
        (10, 30, True, 5),
        # Test interval by zbin axis position
        (12.5, 22.5, True, 5),
    ],
)
def test_build_average_environmental_layer(
    SLD_total_no_substrate,
    zbin_from_param,
    zbin_to_param,
    distance_input_param,
    zbin_interval_param,
):

    environment_layers = build_average_environmental_layer(
        SLD_total_no_substrate,
        zbin_from_param,
        zbin_to_param,
        distance_input=distance_input_param,
        zbin_interval=zbin_interval_param,
    )

    assert type(environment_layers) is list
    assert len(environment_layers[0]) == 4
    assert environment_layers[0][0] == 4.0e-8
    assert environment_layers[0][1] == -3.0e-14
    assert environment_layers[0][2] == 0.0


@pytest.fixture
def substrate_layers():
    substrate_layers = [
        (6.0e-08, -1.0e-10, 0, 0.0),
        (4e-08, -2.0e-12, 10, 0.0),
        (2e-08, -5.0e-14, 8, 0.0),
    ]
    return substrate_layers


@pytest.fixture
def environment_layers():
    environment_layers = [(4.0e-8, -3.0e-14, 0.0, 0.0)]
    return environment_layers


def test_build_SLD_left(SLD_total_no_substrate, substrate_layers, environment_layers):
    first_point_zbin_id = 1
    last_point_zbin_id = 5

    SLD_tuned_left_true_values = (
        np.array(
            [
                6.0e-08,
                4.0e-08,
                2.0e-08,
                1.0e-07,
                5.0e-08,
                3.0e-08,
                5.0e-08,
                3.0e-08,
                4.0e-08,
            ]
        ),
        np.array(
            [
                -1.0e-10,
                -2.0e-12,
                -5.0e-14,
                -1.0e-10,
                -2.0e-14,
                -4.0e-14,
                -2.0e-14,
                -4.0e-14,
                -3.0e-14,
            ]
        ),
    )

    SLD_tuned_left = build_SLD_left(
        SLD_total_no_substrate,
        first_point_zbin_id,
        last_point_zbin_id,
        substrate_layers,
        environment_layers,
    )

    assert len(SLD_tuned_left) == 2
    assert len(SLD_tuned_left[0]) == (
        len(substrate_layers)
        + (last_point_zbin_id - first_point_zbin_id + 1)
        + len(environment_layers)
    )
    assert len(SLD_tuned_left[1]) == (
        len(substrate_layers)
        + (last_point_zbin_id - first_point_zbin_id + 1)
        + len(environment_layers)
    )

    for part in range(2):
        for item in range(len(SLD_tuned_left[part])):
            assert SLD_tuned_left[part][item] == pytest.approx(
                SLD_tuned_left_true_values[part][item], rel=1e-3
            )


def test_build_SLD_right(SLD_total_no_substrate, substrate_layers, environment_layers):
    first_point_zbin_id = 2
    last_point_zbin_id = 6

    SLD_tuned_right_true_values = (
        np.array(
            [
                4.0e-08,
                5.0e-08,
                3.0e-08,
                5.0e-08,
                3.0e-08,
                1.0e-08,
                2.0e-08,
                4.0e-08,
                6.0e-08,
            ]
        ),
        np.array(
            [
                -3.0e-14,
                -2.0e-14,
                -4.0e-14,
                -2.0e-14,
                -4.0e-14,
                -1.0e-10,
                -5.0e-14,
                -2.0e-12,
                -1.0e-10,
            ]
        ),
    )

    SLD_tuned_right = build_SLD_right(
        SLD_total_no_substrate,
        first_point_zbin_id,
        last_point_zbin_id,
        substrate_layers,
        environment_layers,
    )

    assert len(SLD_tuned_right) == 2
    assert len(SLD_tuned_right[0]) == (
        len(substrate_layers)
        + (last_point_zbin_id - first_point_zbin_id + 1)
        + len(environment_layers)
    )
    assert len(SLD_tuned_right[1]) == (
        len(substrate_layers)
        + (last_point_zbin_id - first_point_zbin_id + 1)
        + len(environment_layers)
    )

    for part in range(2):
        for item in range(len(SLD_tuned_right[part])):
            assert SLD_tuned_right[part][item] == pytest.approx(
                SLD_tuned_right_true_values[part][item], rel=1e-3
            )
