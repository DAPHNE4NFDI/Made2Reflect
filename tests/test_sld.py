import pytest


from md2reflect import (
    calculate_SLD_elements,
    calculate_SLD_atom_types,
    calculate_SLD_total,
    calculate_SLD_total_atom_types,
    add_SLD_atom_types,
)


@pytest.fixture
def bc_table_elements_xray_true_values():
    bc_table_elements_xray_true_values = {
        "hydrogen": ("H", 2.818, -1.855e-06),
        "carbon": ("C", 16.94, -0.01661),
        "nitrogen": ("N", 19.79, -0.03213),
    }
    return bc_table_elements_xray_true_values


def test_calculate_SLD_elements(bc_table_elements_xray_true_values):
    number_density_elements_allframes = {
        "hydrogen": [0.00015, 0.00015, 0.00015, 0.0],
        "carbon": [0.00008, 0.00008, 0.00008, 0.0],
        "nitrogen": [0.00002, 0.00002, 0.00002, 0.0],
    }

    SLD_elements_true_values = {
        "hydrogen": (
            [4.227e-09, 4.227e-09, 4.227e-09, 0.0],
            [-2.783e-15, -2.783e-15, -2.783e-15, -0.0],
        ),
        "carbon": (
            [1.355e-08, 1.355e-08, 1.355e-08, 0.0],
            [-1.329e-11, -1.329e-11, -1.329e-11, -0.0],
        ),
        "nitrogen": (
            [3.958e-09, 3.958e-09, 3.958e-09, 0.0],
            [-6.426e-12, -6.426e-12, -6.426e-12, -0.0],
        ),
    }

    SLD_elements = calculate_SLD_elements(
        number_density_elements_allframes, bc_table_elements_xray_true_values
    )

    assert len(SLD_elements) == len(number_density_elements_allframes)
    assert SLD_elements.keys() == number_density_elements_allframes.keys()

    for element in SLD_elements:
        for part in range(2):
            assert len(SLD_elements[element][part]) == len(
                number_density_elements_allframes[element]
            )
            for item in range(len(SLD_elements_true_values[element][part])):
                assert SLD_elements[element][part][item] == pytest.approx(
                    SLD_elements_true_values[element][part][item], rel=1e-3
                )


def test_calculate_SLD_atom_types():
    number_density_atom_types_allframes = {
        "watr": {
            "H1": [0.00001, 0.0, 0.00002, 0.0],
            "H2": [0.00001, 0.0, 0.00002, 0.0],
            "O1": [0.00001, 0.0, 0.00002, 0.0],
        }
    }
    bc_table_atom_types_xray = {
        "watr": {
            "H1": ("H", 2.818, -1.855e-06),
            "H2": ("H", 2.818, -1.855e-06),
            "O1": ("O", 22.64, -0.05946),
        }
    }

    SLD_atom_types_true_values = {
        "watr": {
            "H1": (
                [2.818e-10, 0.0, 5.636e-10, 0.0],
                [-1.855e-16, -0.0, -3.710e-16, -0.0],
            ),
            "H2": (
                [2.818e-10, 0.0, 5.636e-10, 0.0],
                [-1.855e-16, -0.0, -3.710e-16, -0.0],
            ),
            "O1": (
                [2.264e-09, 0.0, 4.528e-09, 0.0],
                [-5.946e-12, -0.0, -1.189e-11, -0.0],
            ),
        }
    }

    SLD_atom_types = calculate_SLD_atom_types(
        number_density_atom_types_allframes, bc_table_atom_types_xray
    )

    assert len(SLD_atom_types) == len(number_density_atom_types_allframes)
    assert SLD_atom_types.keys() == number_density_atom_types_allframes.keys()

    for molecule in SLD_atom_types:
        assert len(SLD_atom_types[molecule]) == len(
            number_density_atom_types_allframes[molecule]
        )
        assert SLD_atom_types[molecule].keys() == (
            number_density_atom_types_allframes[molecule].keys()
        )

        for atom in SLD_atom_types[molecule]:
            for part in range(2):
                assert len(SLD_atom_types[molecule][atom][part]) == len(
                    number_density_atom_types_allframes[molecule][atom]
                )
                for item in range(len(SLD_atom_types[molecule][atom][part])):
                    assert SLD_atom_types[molecule][atom][part][item] == (
                        pytest.approx(
                            SLD_atom_types_true_values[molecule][atom][part][item],
                            rel=1e-3,
                        )
                    )


def test_calculate_SLD_total():
    SLD_elements = {
        "hydrogen": ([3e-09, 3e-09, 3e-09, 0.0], [-3e-15, -3e-15, -3e-15, -0.0]),
        "carbon": ([1e-08, 1e-08, 1e-08, 0.0], [-1e-11, -1e-11, -1e-11, -0.0]),
        "nitrogen": ([4e-09, 4e-09, 4e-09, 0.0], [-6e-12, -6e-12, -6 - 12, -0.0]),
    }
    substrate_element = "carbon"

    SLD_total_true_values = (
        [1.7e-08, 1.7e-08, 1.7e-08, 0.0],
        [-1.6e-11, -1.6e-11, -18.0, 0.0],
    )
    SLD_total_no_substrate_true_values = (
        [7e-09, 7e-09, 7e-09, 0.0],
        [-6.0e-12, -6.0e-12, -18.0, 0.0],
    )

    SLD_total, SLD_total_no_substrate = calculate_SLD_total(
        SLD_elements, substrate_element
    )

    first_element = list(SLD_elements.keys())[0]
    assert len(SLD_total) == len(SLD_elements[first_element])
    assert len(SLD_total_no_substrate) == len(SLD_elements[first_element])

    for part in range(2):
        for item in range(len(SLD_elements[first_element])):
            assert SLD_total[part][item] == pytest.approx(
                SLD_total_true_values[part][item], rel=1e-3
            )
            assert SLD_total_no_substrate[part][item] == pytest.approx(
                SLD_total_no_substrate_true_values[part][item], rel=1e-3
            )


def test_calculate_SLD_total_atom_types():
    SLD_atom_types = {
        "watr": {
            "H1": ([3e-10, 0.0, 6e-10, 0.0], [-2e-16, -0.0, -4e-16, -0.0]),
            "H2": ([3e-10, 0.0, 6e-10, 0.0], [-2e-16, -0.0, -4e-16, -0.0]),
            "O1": ([2e-09, 0.0, 4e-09, 0.0], [-5e-12, -0.0, -1e-11, -0.0]),
        },
        "subs": {"Cu": ([4e-10, 0.0, 8e-10, 0.0], [-1e-14, -0.0, -2e-14, -0.0])},
    }
    substrate_residues = "subs"

    SLD_total_true_values = ([3e-09, 0.0, 6e-09, 0.0], [-5.0e-12, 0.0, -1.0e-11, 0.0])
    SLD_total_no_substrate_true_values = (
        [2.6e-09, 0.0, 5.2e-09, 0.0],
        [-5.00e-12, 0.0, -1.00e-11, 0.0],
    )

    SLD_total, SLD_total_no_substrate = calculate_SLD_total_atom_types(
        SLD_atom_types, substrate_residues
    )

    first_molecule = list(SLD_atom_types.keys())[0]
    first_atom = list(SLD_atom_types[first_molecule].keys())[0]

    assert len(SLD_total) == len(SLD_atom_types[first_molecule][first_atom])
    assert len(SLD_total_no_substrate) == len(
        SLD_atom_types[first_molecule][first_atom]
    )

    for part in range(2):
        assert len(SLD_total[part]) == len(
            SLD_atom_types[first_molecule][first_atom][part]
        )
        assert len(SLD_total_no_substrate[part]) == len(
            SLD_atom_types[first_molecule][first_atom][part]
        )

        for item in range(len(SLD_atom_types[first_molecule][first_atom][part])):
            assert SLD_total[part][item] == pytest.approx(
                SLD_total_true_values[part][item], rel=1e-3
            )
            assert SLD_total_no_substrate[part][item] == pytest.approx(
                SLD_total_no_substrate_true_values[part][item], rel=1e-3
            )


def test_add_SLD_atom_types():
    SLD_atom_types = {
        "watr": {
            "H1": ([3e-10, 0.0, 6e-10, 0.0], [-2e-16, -0.0, -4e-16, -0.0]),
            "H2": ([3e-10, 0.0, 6e-10, 0.0], [-2e-16, -0.0, -4e-16, -0.0]),
            "O1": ([2e-09, 0.0, 4e-09, 0.0], [-5e-12, -0.0, -1e-11, -0.0]),
        },
        "hidr": {"H3": ([0.0, 3e-10, 0.0, 6e-10], [-0.0, -2e-16, -0.0, -4e-16])},
    }
    list_atom_types_add = {"watr": ["H1", "H2"], "hidr": ["H3"]}

    SLD_added_true_values = (
        [6e-10, 3e-10, 1.2e-09, 6e-10],
        [-4e-16, -2e-16, -8e-16, -4e-16],
    )

    SLD_added = add_SLD_atom_types(SLD_atom_types, list_atom_types_add)

    first_molecule = list(SLD_atom_types.keys())[0]
    first_atom = list(SLD_atom_types[first_molecule].keys())[0]

    assert len(SLD_added) == len(SLD_atom_types[first_molecule][first_atom])

    for part in range(2):
        assert len(SLD_added[part]) == len(
            SLD_atom_types[first_molecule][first_atom][part]
        )
        for item in range(len(SLD_atom_types[first_molecule][first_atom][part])):
            assert SLD_added[part][item] == pytest.approx(
                SLD_added_true_values[part][item], rel=1e-3
            )
