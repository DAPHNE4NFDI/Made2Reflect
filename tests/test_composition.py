import os
import pytest

from md2reflect import (
    load_trajectory,
    get_elements,
    find_atom_types,
    find_atom_types_elements,
    isotopic_substitution,
    get_elements_bc_neutrons,
    get_atom_types_bc_neutrons,
    get_elements_b_xrays,
    get_atom_types_b_xrays,
    list_molecules,
)


def test_get_elements():
    traj = load_trajectory(
        os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb")
    )
    elements = get_elements(traj)

    assert len(elements) == 3

    symbol_string = ""
    for item in elements:
        symbol_string += item.symbol

    assert symbol_string == "HCN"


def test_find_atom_types():
    traj = load_trajectory(
        os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb")
    )
    atom_types = find_atom_types(traj)

    assert list(atom_types.keys()) == ["bmim"]
    assert atom_types["bmim"] == [
        "C1",
        "C10",
        "C2",
        "C3",
        "C4",
        "C5",
        "C6",
        "C7",
        "H",
        "H11",
        "H12",
        "H13",
        "H14",
        "H15",
        "H16",
        "H17",
        "H18",
        "H19",
        "H20",
        "H21",
        "H22",
        "H23",
        "H24",
        "N8",
        "N9",
    ]


def test_find_atom_types_elements():
    traj = load_trajectory(
        os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb")
    )
    atom_types_elements = find_atom_types_elements(traj)

    assert list(atom_types_elements.keys()) == ["bmim"]
    assert len(atom_types_elements["bmim"]) == 25
    for atom in atom_types_elements["bmim"]:
        assert atom_types_elements["bmim"][atom] in "HCN"


def test_isotopic_substitution():
    traj = load_trajectory(
        os.path.join("tests", "test_simulations", "traj3_3mols_4frames.pdb")
    )
    atom_types_elements = find_atom_types_elements(traj)

    list_atoms_to_deuterate = ["H11", "H14", "H15", "H16", "H17", "H20"]

    # Check that the 6 deuterated atoms are hydrogen before and deuterion after
    for atom in list_atoms_to_deuterate:
        assert atom_types_elements["bmim"][atom] == "H"

    deuterated = [
        ("bmim", "H20", "D"),
        ("bmim", "H11", "D"),
        ("bmim", "H14", "D"),
        ("bmim", "H15", "D"),
        ("bmim", "H16", "D"),
        ("bmim", "H17", "D"),
    ]

    atom_types_elements = isotopic_substitution(traj, deuterated)

    for atom in list_atoms_to_deuterate:
        assert atom_types_elements["bmim"][atom] == "D"

    # Check that the rest of the atoms are hydrogen, carbon or nitrogen
    for atom in atom_types_elements["bmim"]:
        if atom not in list_atoms_to_deuterate:
            assert atom_types_elements["bmim"][atom] in "HCN"


def test_get_elements_bc_neutrons():
    traj = load_trajectory(
        os.path.join("tests", "test_simulations", "traj3_3mols_1frame.pdb")
    )
    bc_table_true_values = {
        "hydrogen": ("H", -3.74, 0.0),
        "carbon": ("C", 6.65, 0.0),
        "nitrogen": ("N", 9.36, 0.0),
    }

    bc_table = get_elements_bc_neutrons(traj)

    all_elements = list(bc_table.keys())
    for element_key in all_elements:
        assert element_key in "carbon, hydrogen, nitrogen"

        assert bc_table[element_key][0] in "HCN"
        assert len(bc_table[element_key]) >= 3

    for element in bc_table_true_values:
        assert bc_table[element][0] == bc_table_true_values[element][0]
        assert bc_table[element][1] == pytest.approx(
            bc_table_true_values[element][1], rel=1e-3
        )
        assert bc_table[element][2] == pytest.approx(
            bc_table_true_values[element][2], rel=1e-3
        )


def test_get_atom_types_bc_neutrons():
    list_atom_types_elements = {
        "watr": {"H1": "H", "H2": "H", "O1": "O"},
        "acl3": {
            "C1": "C",
            "C2": "C",
            "N1": "N",
            "Cl1": "Cl",
            "Cl2": "Cl",
            "Cl3": "Cl",
        },
    }
    bc_table_true_values = {
        "watr": {
            "H1": ("H", -3.74, 0.0),
            "H2": ("H", -3.74, 0.0),
            "O1": ("O", 5.81, 0.0),
        },
        "acl3": {
            "C1": ("C", 6.65, 0.0),
            "C2": ("C", 6.65, 0.0),
            "N1": ("N", 9.36, 0.0),
            "Cl1": ("Cl", 9.58, 0.0),
            "Cl2": ("Cl", 9.58, 0.0),
            "Cl3": ("Cl", 9.58, 0.0),
        },
    }

    bc_table = get_atom_types_bc_neutrons(list_atom_types_elements)

    assert len(bc_table) == 2
    assert bc_table.keys() == bc_table_true_values.keys()

    for molecule in list_atom_types_elements:
        for atom_type in list_atom_types_elements[molecule]:
            assert bc_table[molecule][atom_type][0] == (
                bc_table_true_values[molecule][atom_type][0]
            )
            assert bc_table[molecule][atom_type][1] == pytest.approx(
                bc_table_true_values[molecule][atom_type][1], rel=1e-3
            )
            assert bc_table[molecule][atom_type][2] == pytest.approx(
                bc_table_true_values[molecule][atom_type][2], rel=1e-3
            )


@pytest.fixture
def bc_table_elements_xray_true_values():
    bc_table_elements_xray_true_values = {
        "hydrogen": ("H", 2.818, -1.855e-06),
        "carbon": ("C", 16.94, -0.01661),
        "nitrogen": ("N", 19.79, -0.03213),
    }
    return bc_table_elements_xray_true_values


def test_get_elements_b_xrays(bc_table_elements_xray_true_values):
    traj = load_trajectory(
        os.path.join("tests", "test_simulations", "traj3_3mols_1frame.pdb")
    )

    bc_table = get_elements_b_xrays(traj, xray_energy=10)

    assert len(bc_table) == 3

    for element in bc_table_elements_xray_true_values:
        assert bc_table[element][0] == bc_table_elements_xray_true_values[element][0]
        assert bc_table[element][1] == pytest.approx(
            bc_table_elements_xray_true_values[element][1], rel=1e-3
        )
        assert bc_table[element][2] == pytest.approx(
            bc_table_elements_xray_true_values[element][2], rel=1e-3
        )


def test_get_atom_types_b_xrays():
    list_atom_types_elements = {
        "watr": {"H1": "H", "H2": "H", "O1": "O"},
        "acl3": {
            "C1": "C",
            "C2": "C",
            "N1": "N",
            "Cl1": "Cl",
            "Cl2": "Cl",
            "Cl3": "Cl",
        },
    }

    bc_table_true_values = {
        "watr": {
            "H1": ("H", 2.818, -1.855e-06),
            "H2": ("H", 2.818, -1.855e-06),
            "O1": ("O", 22.64, -0.05946),
        },
        "acl3": {
            "C1": ("C", 16.94, -0.01661),
            "C2": ("C", 16.94, -0.01661),
            "N1": ("N", 19.79, -0.03213),
            "Cl1": ("Cl", 48.76, -1.314),
            "Cl2": ("Cl", 48.76, -1.314),
            "Cl3": ("Cl", 48.76, -1.314),
        },
    }

    bc_table = get_atom_types_b_xrays(list_atom_types_elements, xray_energy=10)

    assert len(bc_table) == 2
    assert bc_table.keys() == bc_table_true_values.keys()

    for molecule in list_atom_types_elements:
        for atom_type in list_atom_types_elements[molecule]:
            assert bc_table[molecule][atom_type][0] == (
                bc_table_true_values[molecule][atom_type][0]
            )
            assert bc_table[molecule][atom_type][1] == pytest.approx(
                bc_table_true_values[molecule][atom_type][1], rel=1e-3
            )
            assert bc_table[molecule][atom_type][2] == pytest.approx(
                bc_table_true_values[molecule][atom_type][2], rel=1e-3
            )


def test_list_molecules():
    traj = load_trajectory(
        os.path.join("tests", "test_simulations", "traj3_5mols_4frames.pdb")
    )
    molecules_true_values = sorted([("CU1", 2), ("bmim", 3), ("CU", 2)])

    molecules = sorted(list_molecules(traj))
    assert len(molecules) == 3

    for molecule in range(len(molecules)):
        assert molecules[molecule][0] == molecules_true_values[molecule][0]
        assert molecules[molecule][1] == molecules_true_values[molecule][1]
