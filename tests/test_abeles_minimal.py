import os
import pytest
import numpy as np
import csv

from md2reflect.abeles_minimal import (
    #    Error,
    #    ?abeles_reflectivity,
    simul_reflect_meas_cst_dq_q,
    simul_reflect_meas_monochr,
    #    get_resol_fn_monochr,
    #    get_resolution_function,
    #    check_resol_fn_integral,
    #    prepare_R_convolution,
    theoretical_reflectivity,
    abeles_fresnel_reflectivity,
    abeles_nevo_croce_reflectivity,
    #    prepare_fn_convolution_fast,
    #    prepare_R_convolution_fast,
    #    __interp,
    #    apply_resol_fn,
    #    gauss,
)


def test_abeles_reflectivity():
    """This function should be defined, or fix the name at "prepare_R_convolution"."""
    # abeles_reflectivity(q, sld, sld_abs, thickness, roughness)
    pass


def test_error_class():
    # This function tests the class "Error".
    pass


@pytest.fixture
def reflectivity_constant_resolution_example():
    # Simulated reflectivity as measured at a TOF reflectometer using optically
    # blind choppers (constant dq_q resolution) and a purposefully high
    # background, with the following parameters:

    # Points where we want to evaluate R (Angstrom^-1):
    # q = np.linspace(0,.1,1000)

    # Atmosphere/vacuum: SLD = 0
    # Atmosphere/vacuum-layer roughness sigma = 2
    # Layer: SLD = 2e-6, thickness = 200 Angstrom
    # Layer-substrate roughness sigma = 1
    # Substrate: SLD = 3e-6
    # (We assume the SLD absorption / imaginary part to be zero.)

    # Experimental parameters:
    # dq_q=.06, bckg= 5e-6

    path_csvfile = os.path.join(
        "tests", "test_simulations", "R_cst_dq_q_3-2-0e-6_1-200-2A.csv"
    )
    with open(path_csvfile) as csvfile:
        reader = csv.reader(csvfile, delimiter=",")
        qz_reference = []
        reflectivity_reference = []
        count = 0
        for row in reader:
            if count <= 1:
                count += 1
            else:
                qz_reference += [float(row[0])]
                reflectivity_reference += [float(row[1])]

    reflectivity_constant_resolution_example = [
        np.array(qz_reference),
        np.array(reflectivity_reference),
    ]

    return reflectivity_constant_resolution_example


@pytest.fixture
def reflectivity_monochromatic_example():
    # Simulated reflectivity as measured at a monochromatic instrument and a
    # purposefully high background, with the following parameters:

    # Points where we want to evaluate R (Angstrom^-1):
    # q = np.linspace(0,.1,1000)

    # Atmosphere/vacuum: SLD = 0
    # Atmosphere/vacuum-layer roughness sigma = 2
    # Layer: SLD = 2e-6, thickness = 200 Angstrom
    # Layer-substrate roughness sigma = 1
    # Substrate: SLD = 3e-6
    # (We assume the SLD absorption / imaginary part to be zero.)

    # Experimental parameters:
    # wl = 1, dwl=.03, dtheta=.005, bckg= 5e-6

    path_csvfile = os.path.join(
        "tests", "test_simulations", "R_monochr_3-2-0e-6_1-200-2A.csv"
    )
    with open(path_csvfile) as csvfile:
        reader = csv.reader(csvfile, delimiter=",")
        qz_reference = []
        reflectivity_reference = []
        count = 0
        for row in reader:
            if count <= 1:
                count += 1
            else:
                if row[1] != "":
                    qz_reference += [float(row[0])]
                    reflectivity_reference += [float(row[1])]

    reflectivity_monochromatic_example = [
        np.array(qz_reference),
        np.array(reflectivity_reference),
    ]

    return reflectivity_monochromatic_example


def test_simul_reflect_meas_cst_dq_q(reflectivity_constant_resolution_example):
    # Simulated reflectivity as measured at a TOF reflectometer using optically
    # blind choppers (constant dq_q resolution) and a purposefully high
    # background, with the following parameters:

    # Points where we want to evaluate R (Angstrom^-1):
    # q = np.linspace(0,.1,1000)

    # Atmosphere/vacuum: SLD = 0
    # Atmosphere/vacuum-layer roughness sigma = 2
    # Layer: SLD = 2e-6, thickness = 200 Angstrom
    # Layer-substrate roughness sigma = 1
    # Substrate: SLD = 3e-6
    # (We assume the SLD absorption / imaginary part to be zero.)

    # Experimental parameters:
    # dq_q=.06, bckg= 5e-6

    # We compute the reflectivity for the same qz values as the example
    qz = reflectivity_constant_resolution_example[0]
    sld = np.array([0, 2e-6, 3e-6])
    sld_abs = np.array([0, 0, 0])
    thickness = np.array([0, 200, np.inf])
    roughness = np.array([2, 1.0])
    background = 5e-6
    resolution = 0.06

    reflectivity_calculation = simul_reflect_meas_cst_dq_q(
        qz, sld, sld_abs, thickness, roughness, bckg=background, dq_q=resolution
    )

    assert reflectivity_calculation == pytest.approx(
        np.array(reflectivity_constant_resolution_example[1]), rel=1e-10
    )


def test_simul_reflect_meas_monochr(reflectivity_monochromatic_example):
    # Simulated reflectivity as measured at a monochromatic instrument and a
    # purposefully high background, with the following parameters:

    # Points where we want to evaluate R (Angstrom^-1):
    # q = np.linspace(0,.1,1000)

    # Atmosphere/vacuum: SLD = 0
    # Atmosphere/vacuum-layer roughness sigma = 2
    # Layer: SLD = 2e-6, thickness = 200 Angstrom
    # Layer-substrate roughness sigma = 1
    # Substrate: SLD = 3e-6
    # (We assume the SLD absorption / imaginary part to be zero.)

    # Experimental parameters:
    # wl = 1, dwl=.03, dtheta=.005, bckg= 5e-6

    # We compute the reflectivity for the same qz values as the example
    qz = reflectivity_monochromatic_example[0]
    sld = np.array([0, 2e-6, 3e-6])
    sld_abs = np.array([0, 0, 0])
    thickness = np.array([0, 200, np.inf])
    roughness = np.array([2, 1.0])
    background = 5e-6
    wavelength = 1
    dwavelength = 0.03
    dtheta = 0.005
    background = 5e-6

    reflectivity_calculation = simul_reflect_meas_monochr(
        qz,
        sld,
        sld_abs,
        thickness,
        roughness,
        bckg=background,
        wl=wavelength,
        dwl=dwavelength,
        dtheta=dtheta,
    )

    assert reflectivity_calculation == pytest.approx(
        np.array(reflectivity_monochromatic_example[1]), rel=1e-10
    )


def test_get_resol_fn_monochr():
    # get_resol_fn_monochr(q_meas, wl, dwl, dtheta, npts=200, nsigma=5, verbose=False)
    pass


def test_get_resolution_function():
    # get_resolution_function(q_meas, q_resol_charact, npts=2001, nsigma=5,
    #                         fn="gauss", safe_eval_thresh=0.99, normalize=True,
    #                         verbose=False)
    pass


def test_check_resol_fn_integral():
    # check_resol_fn_integral(fresdx, thresh=0.99, verbose=False)
    pass


def test_prepare_R_convolution():
    # prepare_R_convolution(q_target, sld, sld_abs, thickness, roughness)
    pass


@pytest.fixture
def neutron_reference_smooth_example():
    # Reference neutron reflectivity curve obtained from:
    # https://www.ncnr.nist.gov/instruments/magik/calculators/reflectivity-calculator.html
    # by defining a single layer of 100A and a semi-infinite layer at each side
    # WITHOUT any roughness between them.
    path_tsvfile = os.path.join(
        "tests", "test_simulations", "reflectivity_2-1-0e-6_0-100-0A.tsv"
    )
    with open(path_tsvfile) as tsvfile:
        reader = csv.reader(tsvfile, delimiter="\t")
        qz_reference = []
        reflectivity_reference = []
        count = 0
        for row in reader:
            if count <= 1:
                count += 1
            else:
                qz_reference += [float(row[0])]
                reflectivity_reference += [float(row[1])]

    neutron_reference_smooth_example = [
        np.array(qz_reference),
        np.array(reflectivity_reference),
    ]

    return neutron_reference_smooth_example


@pytest.fixture
def neutron_reference_roughness_example():
    # Reference neutron reflectivity curve obtained from:
    # https://www.ncnr.nist.gov/instruments/magik/calculators/reflectivity-calculator.html
    # by defining a single layer of 100A and a semi-infinite layer at each side
    # WITH certain roughness between them.
    path_tsvfile = os.path.join(
        "tests", "test_simulations", "reflectivity_2-1-0e-6_4-100-7A.tsv"
    )
    with open(path_tsvfile) as tsvfile:
        reader = csv.reader(tsvfile, delimiter="\t")
        qz_reference = []
        reflectivity_reference = []
        count = 0
        for row in reader:
            if count <= 1:
                count += 1
            else:
                qz_reference += [float(row[0])]
                reflectivity_reference += [float(row[1])]

    neutron_reference_roughness_example = [
        np.array(qz_reference),
        np.array(reflectivity_reference),
    ]

    return neutron_reference_roughness_example


def test_theoretical_reflectivity(neutron_reference_roughness_example):
    # Comparison with the reference neutron reflectivity curve obtained from:
    # https://www.ncnr.nist.gov/instruments/magik/calculators/reflectivity-calculator.html
    # by defining a single layer of 100A and a semi-infinite layer at each side
    # WITH certain roughness between them.

    # We compute the reflectivity for the same qz values as the reference
    qz = neutron_reference_roughness_example[0]
    sld = np.array([0.0, 1e-6, 2e-6])
    sld_abs = np.array([0, 0, 0.0])
    thickness = np.array([0, 100.0, np.inf])
    roughness = np.array([7, 4.0])

    reflectivity_calculation = theoretical_reflectivity(
        qz, sld, sld_abs, thickness, roughness
    )

    assert reflectivity_calculation == pytest.approx(
        np.array(neutron_reference_roughness_example[1]), rel=1e-10
    )


def test_abeles_fresnel_reflectivity(neutron_reference_smooth_example):
    # Comparison with the reference neutron reflectivity curve obtained from:
    # https://www.ncnr.nist.gov/instruments/magik/calculators/reflectivity-calculator.html
    # by defining a single layer of 100A and a semi-infinite layer at each side
    # WITHOUT any roughness between them.

    # We compute the reflectivity for the same qz values as the reference
    qz = neutron_reference_smooth_example[0]
    sld = np.array([0.0, 1e-6, 2e-6])
    sld_abs = np.array([0, 0, 0.0])
    thickness = np.array([0, 100.0, np.inf])

    reflectivity_calculation = (
        abs(abeles_fresnel_reflectivity(qz, sld, sld_abs, thickness)) ** 2
    )

    assert reflectivity_calculation == pytest.approx(
        np.array(neutron_reference_smooth_example[1]), rel=1e-10
    )


def test_abeles_nevo_croce_reflectivity(neutron_reference_roughness_example):
    # We compute the reflectivity for the same qz values as the reference
    qz = neutron_reference_roughness_example[0]
    sld = np.array([0.0, 1e-6, 2e-6])
    sld_abs = np.array([0, 0, 0.0])
    thickness = np.array([0, 100.0, np.inf])
    roughness = np.array([7, 4.0])

    reflectivity_calculation = (
        abs(abeles_nevo_croce_reflectivity(qz, sld, sld_abs, thickness, roughness)) ** 2
    )

    assert reflectivity_calculation == pytest.approx(
        np.array(neutron_reference_roughness_example[1]), rel=1e-10
    )


def test_prepare_fn_convolution_fast():
    # prepare_fn_convolution_fast(fn, q_meas, q_target, *args, **kwargs)
    pass


def test_prepare_R_convolution_fast():
    # prepare_R_convolution_fast(q_meas, q_target, sld, sld_abs, thickness, roughness)
    pass


def test__interp():
    #    x = np.arange(10)
    #    y = np.arange(10)
    #    target = np.arange(0.5, 10, 0.5)
    #    assert __interp(x, y, target) == 2 * target
    # __interp(x, y, target)
    pass


def test_apply_resol_fn():
    # apply_resol_fn(R, resol_fn)
    pass


def test_gauss():
    # gauss(x)
    pass
