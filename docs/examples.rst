Examples
========

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   /examples/md2reflect_examples_minimal_neutron
   /examples/md2reflect_examples_minimal_neutron_low_resources.ipynb
   /examples/md2reflect_examples_loading_simulations.ipynb
   /examples/md2reflect_examples_number_density.ipynb
   /examples/md2reflect_examples_isotopic_substitution.ipynb
   /examples/md2reflect_examples_SLD.ipynb
   /examples/md2reflect_examples_layers_reflectivity.ipynb
   /examples/abeles_minimal_example.ipynb

   
Download the Jupyter notebooks below:  
 * :download:`Minimal Example - Neutron Reflectivity <../examples/md2reflect_examples_minimal_neutron.ipynb>`
 * :download:`Minimal Example - Neutron Reflectivity with Low Resources <../examples/md2reflect_examples_minimal_neutron_low_resources.ipynb>`
 * :download:`Loading simulations <../examples/md2reflect_examples_loading_simulations.ipynb>`
 * :download:`Atomic number density of simulation <../examples/md2reflect_examples_number_density.ipynb>`
 * :download:`Isotopic substitution <../examples/md2reflect_examples_isotopic_substitution.ipynb>`
 * :download:`Scattering length density <../examples/md2reflect_examples_SLD.ipynb>`
 * :download:`Adding theoretical layers to the simulation and calculating reflectivity <../examples/md2reflect_examples_layers_reflectivity.ipynb>`
 * :download:`Abeles reflectivity calculation functions <../examples/abeles_minimal_example.ipynb>`
