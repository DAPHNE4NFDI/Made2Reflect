Installation
============


MD2reflect makes use of a series of Python libraries. In order to ensure it 
works correctly we recommend installing those versions of the modules 
(although it may work with other versions as well):

   * Python         3.7.5
   * Numpy          1.18.0
   * Matplotlib     3.1.2
   * IPython        7.11.0
   * Periodictable  1.5.2
   * MDTraj         1.9.3
   * Pymol          2.2.0

If you can run the module but are getting some errors, you can try using
the function :func:`md2reflect.check_compatibility()` to check if the versions installed 
in your system coincide with these.

A Docker image can be built with all the necessary dependencies to run the 
package. The necessary files and the instructions for linux can be found in the 
"docker-files" folder of the repository.

