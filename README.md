# Made2Reflect (v0.15)

Made2Reflect is a scientific software written in Python 3 for the calculation of 
neutron and x-ray reflectivity profiles from molecular dynamic simulations. It 
is a project of the Helmholtz-Zentrum Geesthacht, German Engineering Materials 
Science Centre (GEMS) at Heinz Maier-Leibnitz Zentrum (MLZ), in Garching near 
Munich.

The program reads molecular dynamics computer simulation trajectories, can 
perform isotopic exchange on any groups of atoms, and calculates the scattering 
length density for neutron scattering or the electron density for x-ray 
scattering in bins along the z-axis. Two semi-infinite edges (substrate and 
bulk) must be added to the scattering length or electron density obtained from 
the simulations, which may contain layered structures and be smeared with a 
substrate roughness specified by the user. From this, the expected reflectivity 
curve can be calculated.

Since it uses the library MDTraj to load the files, it is compatible with a 
large range of trajectory file formats (DCD, PDB…).

Documentation and installation instructions are available at
https://alberginia.gitlab.io/Made2Reflect/

The code is freely available under the GNU GPL License.


### INSTALLATION

Made2Reflect makes use of a series of Python libraries. In order to ensure it 
works correctly we recommend installing those versions of the modules 
(although it may work with other versions as well):

    Python         3.7.5
    Numpy          1.18.0
    Matplotlib     3.1.2
    IPython        7.11.0
    Periodictable  1.5.2
    MDTraj         1.9.3
    Pymol          2.2.0

A Docker image can be built with all the necessary dependencies to run the 
package. The necessary files and the instructions for linux can be found in the 
"docker-files" folder of the repository.

The Sphinx documentation can be rebuilt by running "make html" in the docs 
folder. Jupyter notebooks included in the documentation must be updated in the 
"docs/examples" folder. The index file as well as the rest of the documentation 
files can be found under the "docs/_build/html" folder.


### CONTRIBUTION

This project uses pre-commits to unify the code style.

If you want to contribute, please follow these steps:
1. Install pre-commit in your system:  
    `pip install pre-commit`
  
2. Activate pre-commit for this project from the local repository folder (if you use worktree you will have to do this for every branch):  
    `pre-commit install`

After that, files which are changed and added to the stage will be checked with black and flake8 when trying to commit those changes. If the file is not black compliant, it will be reformatted accordingly and will have to be staged again in order to commit it with those changes. If the file is not flake8 compliant, the warning/s will be shown and the commit will not take place. The file will have to be modified manually in order to fix the flake8 warnings and be able to commit any changes.
